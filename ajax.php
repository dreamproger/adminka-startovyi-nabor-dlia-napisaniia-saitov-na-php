<?php 
//------------------------------------------------------------------------
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
//------------------------------------------------------------------------
include(__DIR__.'/admin/settings.php');
$GLOBALS['folder'] = __DIR__ . '/';
$GLOBALS['url_full'] = $GLOBALS['constructHTTP'];
$GLOBALS['url_short'] = '/';
include(__DIR__.'/admin/datebase.php');
include(__DIR__.'/admin/libs/functions.php');
//------------------------------------------------------------------------
$args = $_POST;
if (($args['phone'] ?? '') == '') exit(json_encode(array('response' => 0, 'error' => 'Не указаны все поля6')));
if (($args['name'] ?? '') == '') exit(json_encode(array('response' => 0, 'error' => 'Не указаны все поля8')));
if (($args['description'] ?? '') == '') exit(json_encode(array('response' => 0, 'error' => 'Не указаны все поля8')));
// Очищаем XSS
$args['phone'] = htmlentities($args['phone']);
$args['name'] = htmlentities($args['name']);
$args['description'] = htmlentities($args['description']);
// Описания
$opis = '';
$opis .= 'Номер телефона: ' . ($args['phone']) . "\n";
$opis .= 'Имя: ' . ($args['name']) . "\n";
$opis .= 'Описание: ' . ($args['description']) . "\n";
// Настройки
$from = 'mail@mail.ru';
$to = 'mail@mail.ru';
// Отправка письма
$headers = 'From: ' . $from . "\r\n";
echo(json_encode(array('opis' => $opis, 'response' => mail($to, 'Заявка с сайта', 'Новая заявка с сайта' . "\n" . $opis . " \n Время: " . date("Y-m-d H:i:s", time()), $headers) )));
//------------------------------------------------------------------------
?>
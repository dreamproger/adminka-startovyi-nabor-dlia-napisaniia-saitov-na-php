function onLoadObjects(){
	if (mapInited) initMap();
}
var mapInited = false;
ymaps.ready(function(){
	mapInited = true;
	initMap();
});

function initMap(){
	var maps = $('.geoMapEdit:not(.inited)');
	maps.each(initMapView);
	var maps = $('.geoMapShow:not(.inited)');
	maps.each(initMapShow);

}

function initMapView(){
	var map = $(this);  var placemark = null; var coordsG; map.addClass('inited');
	map.css('width', '100%');
	map.css('height', '400px');
	var nCoord = [48.701144, 66.465172];
	var nZoom = 5;
	if (map.attr("data-apt") != "null") nCoord = [map.attr('data-apt'), map.attr('data-lon')];
	if (map.attr('data-zoom') != "null") nZoom = map.attr('data-zoom');
	myMap = new ymaps.Map(this, {
        center: nCoord,
        zoom: nZoom
    });
    if (map.attr("data-apt") != "null"){
    	coordsG = [map.attr('data-apt'), map.attr('data-lon')];
    	placemark = new  ymaps.Placemark(coordsG, {}, {draggable: true,  iconColor: 'red'});
    	myMap.geoObjects.add(placemark);
    	placemark.events.add('dragend', function (e) {
       		coordsG = e.get('target').geometry.getCoordinates();
    	});
    }
	myMap.events.add('click', function (e) {
		if (placemark != null) {
			myMap.geoObjects.remove(placemark);
		}
		var coords = e.get('coords');
		coordsG = coords;
		placemark = new  ymaps.Placemark(coords, {}, {draggable: true,  iconColor: 'red'});
		myMap.geoObjects.add(placemark);
		placemark.events.add('dragend', function (e) {
       		coordsG = e.get('target').geometry.getCoordinates();
    	});
	});
	this.getCoordinates = function(){
		if (coordsG == null) return null;
		return JSON.stringify({aptitude: coordsG[0], longtitude: coordsG[1], zoom: myMap.getZoom()});
	}
}
function initMapShow(){
	var map = $(this);  var placemark = null; var coordsG; map.addClass('inited');
	map.css('width', '100%');
	map.css('height', '400px');
	var nCoord = [48.701144, 66.465172];
	var nZoom = 5;
	if (map.attr("data-apt") != "null") nCoord = [map.attr('data-apt'), map.attr('data-lon')];
	if (map.attr('data-zoom') != "null") nZoom = map.attr('data-zoom');
	myMap = new ymaps.Map(this, {
        center: nCoord,
        zoom: nZoom
    });  
    placemark = new  ymaps.Placemark(nCoord);
    myMap.geoObjects.add(placemark);
}
<?php
	/*
		Санат Капышев:
			Функции для чтения записей из БД
			Дата создания: 27 мая 2015
			Дата последнего изменения: 27 мая 2015

		Apple Cider Studios 2015(C)

	*/
//-------------------------------------------------------------------------------------------------------------------------------
	$count = 1;
	function oth_reader_query($query, $args) {
		/* Все запросы */
		//--------------------------------------
		$link = start_connection_read();
		//--------------------------------------
		foreach ($args as $key => $value) {
			$args[$key] = strip_tags($args[$key]);
			$args[$key] = mysqli_real_escape_string($link, $args[$key]);
		}
		//--------------------------------------
		for ($index = 0; true; $index++) {
			//-------------------------
			$i = stripos($query, '%i');
			$s = stripos($query, '%s');
			//-------------------------
			if (($i == false) & ($s == false)) { break; }
			if ($i == false) { $i = 99999; }
			if ($s == false) { $s = 99999; }
			//-------------------------
			if ($i > $s) {
				
				$query =  preg_replace('/%s/', '"' . $args[$index] . '"', $query, 1);
			} else {

				if (!is_numeric($args[$index])) { $args[$index] = 0; }
				$query =  preg_replace('/%i/', '"' . $args[$index] . '"', $query, 1);
			}
			//-------------------------
		}
		//--------------------------------------
		$result = mysqli_query($link, $query);
		if ($result === false) echo('Запрос не удался: ' . mysqli_error($link));
		close_connection($link);
		return $result;
		//--------------------------------------
	}
 	function CheckString($string){
 		return mysqli_real_escape_string($GLOBALS['link'], $string);
 	}
//-------------------------------------------------------------------------------------------------------------------------------
	function arr_query($query,  ...$args) {
		/* Возвращает результаты в виде массива PHP */
		$otvet = array();
		$result = oth_reader_query($query, $args);
		while ($line = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
			array_push($otvet, $line);
		}
		return $otvet;
	}	
 
//-------------------------------------------------------------------------------------------------------------------------------
	function arr($table, ...$args){
		/* Возвращает результаты поиска в таблице $table с поиском по значениям $args */
		//--------------------------------------
		$table = $GLOBALS['mysql_prefix'] . $table;
		$table = mysqli_real_escape_string($GLOBALS['link'], $table);
		//--------------------------------------
		foreach ($args as $key => $value) {
			$args[$key] = strip_tags($args[$key]);
			$args[$key] = mysqli_real_escape_string($GLOBALS['link'], $args[$key]);
		}
		//--------------------------------------
		$where = '1 ';
		for ($i=0; $i < count($args) ; $i = $i + 2) { 
			$where = $where . ' AND `' . $args[$i] . '` = "' . $args[$i + 1] . '"';
		}
		//--------------------------------------
		$sortBy = "";
		$has = arr_query("DESCRIBE `" . $table . "`");
		foreach ($has as $fld) if($fld['Field'] == 'Priority') $sortBy = " ORDER BY `Priority`, `ID` DESC";
		//--------------------------------------
		$query = 'SELECT * FROM `' . $table . '` WHERE ' . $where . $sortBy;
		$result = oth_reader_query($query, array() );
		
		//--------------------------------------
		$otvet = array();
		while ($line = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
			array_push($otvet, $line);
		}
		//--------------------------------------
		return $otvet;
		//--------------------------------------
	}
	function arrz($table, ...$args){
		/* Возвращает результаты поиска в таблице $table с поиском по значениям $args */
		//--------------------------------------
		$table = $GLOBALS['mysql_prefix'] . $table;
		$table = mysqli_real_escape_string($GLOBALS['link'], $table);
		//--------------------------------------
		foreach ($args as $key => $value) {
			$args[$key] = strip_tags($args[$key]);
			$args[$key] = mysqli_real_escape_string($GLOBALS['link'], $args[$key]);
		}
		//--------------------------------------
		$where = '1 ';
		for ($i=0; $i < count($args) ; $i = $i + 2) { 
			$where = $where . ' AND `' . $args[$i] . '` = "' . $args[$i + 1] . '"';
		}
		//--------------------------------------
		$sortBy = "";
		$has = arr_query("DESCRIBE `" . $table . "`");
		foreach ($has as $fld) if($fld['Field'] == 'Priority') $sortBy = " ORDER BY `Priority` DESC, `ID`";
		//--------------------------------------
		$query = 'SELECT * FROM `' . $table . '` WHERE ' . $where . $sortBy;
		$result = oth_reader_query($query, array() );
		
		//--------------------------------------
		$otvet = array();
		while ($line = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
			array_push($otvet, $line);
		}
		//--------------------------------------
		return $otvet;
		//--------------------------------------
	}
	function arrd($table, ...$args){
		/* Возвращает результаты поиска в таблице $table с поиском по значениям $args */
		//--------------------------------------
		$table = $GLOBALS['mysql_prefix'] . $table;
		$table = mysqli_real_escape_string($GLOBALS['link'], $table);
		//--------------------------------------
		foreach ($args as $key => $value) {
			$args[$key] = strip_tags($args[$key]);
			$args[$key] = mysqli_real_escape_string($GLOBALS['link'], $args[$key]);
		}
		//--------------------------------------
		$where = '1 ';
		for ($i=0; $i < count($args) ; $i = $i + 2) { 
			$where = $where . ' AND `' . $args[$i] . '` = "' . $args[$i + 1] . '"';
		}
		//--------------------------------------
		$query = 'SELECT * FROM `' . $table . '` WHERE ' . $where . $sortBy;
		$result = oth_reader_query($query, array() );
		
		//--------------------------------------
		$otvet = array();
		while ($line = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
			array_push($otvet, $line);
		}
		//--------------------------------------
		return $otvet;
		//--------------------------------------
	}
//-------------------------------------------------------------------------------------------------------------------------------
	function arr_order($table, $order, ...$args){
		/* Возвращает результаты поиска в таблице $table с поиском по значениям $args */
		//--------------------------------------
		$table = $GLOBALS['mysql_prefix'] . $table;
		$table = mysqli_real_escape_string($GLOBALS['link'], $table);
		//--------------------------------------
		foreach ($args as $key => $value) {
			$args[$key] = strip_tags($args[$key]);
			$args[$key] = mysqli_real_escape_string($GLOBALS['link'], $args[$key]);
		}
		//--------------------------------------
		$where = '1 ';
		for ($i=0; $i < count($args) ; $i = $i + 2) { 
			$where = $where . ' AND `' . $args[$i] . '` = "' . $args[$i + 1] . '"';
		}
		//--------------------------------------
		$query = 'SELECT * FROM `' . $table . '` WHERE ' . $where . ' ORDER BY `' . $order . '`';
		$result = oth_reader_query($query, array() );
		//--------------------------------------
		$otvet = array();
		while ($line = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
			array_push($otvet, $line);
		}
		//--------------------------------------
		return $otvet;
		//--------------------------------------
	}
//-------------------------------------------------------------------------------------------------------------------------------
	function arr_order_desc($table, $order, ...$args){
		/* Возвращает результаты поиска в таблице $table с поиском по значениям $args */
		//--------------------------------------
		$table = $GLOBALS['mysql_prefix'] . $table;
		$table = mysqli_real_escape_string($GLOBALS['link'], $table);
		//--------------------------------------
		foreach ($args as $key => $value) {
			$args[$key] = strip_tags($args[$key]);
			$args[$key] = mysqli_real_escape_string($GLOBALS['link'], $args[$key]);
		}
		//--------------------------------------
		$where = '1 ';
		for ($i=0; $i < count($args) ; $i = $i + 2) { 
			$where = $where . ' AND `' . $args[$i] . '` = "' . $args[$i + 1] . '"';
		}
		//--------------------------------------
		$query = 'SELECT * FROM `' . $table . '` WHERE ' . $where . ' ORDER BY `' . $order . '` DESC';
		$result = oth_reader_query($query, array() );
		//--------------------------------------
		$otvet = array();
		while ($line = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
			array_push($otvet, $line);
		}
		//--------------------------------------
		return $otvet;
		//--------------------------------------
	}
//-------------------------------------------------------------------------------------------------------------------------------
	function get_query($query, ...$args) {
		/* Возвращает первый результат запроса */
		$otvet = array();
		$result = oth_reader_query($query, $args);
		$line = mysqli_fetch_array($result, MYSQLI_ASSOC);
		return $line;
	}
//-------------------------------------------------------------------------------------------------------------------------------
	function get($table, ...$args){
		/* Возвращает первый результат поиска в таблице $table с поиском по значениям $args */
		//--------------------------------------
		$table = $GLOBALS['mysql_prefix'] . $table;
		$i=0;
		$where = '1 ';
		//--------------------------------------
		if (count($args) % 2 == 1) { 
			/*
			$result = mysqli_query($GLOBALS['link'], 'SHOW INDEX FROM  `' . $table . '`');
			$line = mysqli_fetch_array($result, MYSQLI_ASSOC);
			$key = $line['Column_name'];
			*/
			$key = 'ID';
			$where = '`' . $key . '` = "' . $args[0] . '"';
			$i = 1;
		} 
		//--------------------------------------
		$table = mysqli_real_escape_string($GLOBALS['link'], $table);
		//--------------------------------------
		foreach ($args as $key => $value) {
			$args[$key] = strip_tags($args[$key]);
			$args[$key] = mysqli_real_escape_string($GLOBALS['link'], $args[$key]);
		}
		//--------------------------------------
		for (; $i < count($args) ; $i = $i + 2) { 
			$where = $where . ' AND `' . $args[$i] . '` = "' . $args[$i + 1] . '"';
		}
		//--------------------------------------
		$sortBy = "";
		$has = arr_query("DESCRIBE `" . $table . "`");
		foreach ($has as $fld) if($fld['Field'] == 'Priority') $sortBy = " ORDER BY `Priority` DESC, `ID`";
		//--------------------------------------
		$query = 'SELECT * FROM `' . $table . '` WHERE ' . $where . $sortBy;
		$result = oth_reader_query($query, array());
		//--------------------------------------
		$otvet = array();
		$line = mysqli_fetch_array($result, MYSQLI_ASSOC);
		//--------------------------------------
		return $line;
		//--------------------------------------
	}
//-------------------------------------------------------------------------------------------------------------------------------
	function get_ignorecase($table, ...$args){
		/* Возвращает первый результат поиска в таблице $table с поиском по значениям $args */
		//--------------------------------------
		$table = $GLOBALS['mysql_prefix'] . $table;
		$i=0;
		$where = '1 ';
		//--------------------------------------
		if (count($args) % 2 == 1) { 
			/*
			$result = mysqli_query($GLOBALS['link'], 'SHOW INDEX FROM  `' . $table . '`');
			$line = mysqli_fetch_array($result, MYSQLI_ASSOC);
			$key = $line['Column_name'];
			*/
			$key = 'ID';
			$where = '`' . $key . '` = "' . $args[0] . '"';
			$i = 1;
		} 
		//--------------------------------------
		$table = mysqli_real_escape_string($GLOBALS['link'], $table);
		//--------------------------------------
		foreach ($args as $key => $value) {
			$args[$key] = strip_tags($args[$key]);
			$args[$key] = mysqli_real_escape_string($GLOBALS['link'], $args[$key]);
		}
		//--------------------------------------
		for (; $i < count($args) ; $i = $i + 2) { 
			$where = $where . ' AND LOWER(`' . $args[$i] . '`) = LOWER("' . $args[$i + 1] . '")';
		}
		//--------------------------------------
		$query = 'SELECT * FROM `' . $table . '` WHERE ' . $where;
		$result = oth_reader_query($query, array());
		//--------------------------------------
		$otvet = array();
		$line = mysqli_fetch_array($result, MYSQLI_ASSOC);
		//--------------------------------------
		return $line;
		//--------------------------------------
	}
//-------------------------------------------------------------------------------------------------------------------------------
	function arr_by_page($table, $page, $num, $orderby, $desc, ...$args){
		/* Возвращает первый результат поиска в таблице $table с поиском по значениям $args */
		//--------------------------------------
		$table = $GLOBALS['mysql_prefix'] . $table;
		$i=0;
		$where = '1 ';
		//--------------------------------------
		if (count($args) % 2 == 1) { 
			/*
			$result = mysqli_query($GLOBALS['link'], 'SHOW INDEX FROM  `' . $table . '`');
			$line = mysqli_fetch_array($result, MYSQLI_ASSOC);
			$key = $line['Column_name'];
			*/
			$key = 'ID';
			$where = '`' . $key . '` = "' . $args[0] . '"';
			$i = 1;
		} 
		//--------------------------------------
		$table = mysqli_real_escape_string($GLOBALS['link'], $table);
		//--------------------------------------
		foreach ($args as $key => $value) {
			$args[$key] = strip_tags($args[$key]);
			$args[$key] = mysqli_real_escape_string($GLOBALS['link'], $args[$key]);
		}
		//--------------------------------------
		for (; $i < count($args) ; $i = $i + 2) { 
			$where = $where . ' AND `' . $args[$i] . '` = "' . $args[$i + 1] . '"';
		}
		//--------------------------------------
		$start = ($page - 1) * $num;
		//--------------------------------------
		if ($desc === true) $desc = 'DESC';
		if ($desc === false) $desc = '';
		//--------------------------------------
		$query = 'SELECT * FROM `' . $table . '` WHERE ' . $where . 'ORDER BY `'  . $orderby . '` ' . $desc . ' LIMIT ' . $start . ',' . $num ;
		$result = oth_reader_query($query, array());
		//--------------------------------------
		$otvet = array();
		while ($line = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
			array_push($otvet, $line);
		}
		//--------------------------------------
		return $otvet;
		//--------------------------------------
	}
//-------------------------------------------------------------------------------------------------------------------------------
	function arr_count($table, ...$args){
		$table = $GLOBALS['mysql_prefix'] . $table;
		$table = mysqli_real_escape_string($GLOBALS['link'], $table);
		//------------------------------------
		$i=0;
		$where = '1 ';
		//------------------------------------
		foreach ($args as $key => $value) {
			$args[$key] = strip_tags($args[$key]);
			$args[$key] = mysqli_real_escape_string($GLOBALS['link'], $args[$key]);
		}
		//--------------------------------------
		for (; $i < count($args) ; $i = $i + 2) { 
			$where = $where . ' AND `' . $args[$i] . '` = "' . $args[$i + 1] . '"';
		}
		//------------------------------------
		$query = 'SELECT COUNT(*) FROM `' . $table . '` WHERE ' . $where;
		$result = oth_reader_query($query, array());
		//------------------------------------
		$line = mysqli_fetch_array($result, MYSQLI_ASSOC);
		return($line['COUNT(*)']);
		//------------------------------------
	}
//-------------------------------------------------------------------------------------------------------------------------------
	function getMax($table, $row, ...$args){
		/* Возвращает первый результат поиска в таблице $table с поиском по значениям $args */
		//--------------------------------------
		$table = $GLOBALS['mysql_prefix'] . $table;
		$i=0;
		$where = '1 ';
		//--------------------------------------
		if (count($args) % 2 == 1) { 
			/*
			$result = mysqli_query($GLOBALS['link'], 'SHOW INDEX FROM  `' . $table . '`');
			$line = mysqli_fetch_array($result, MYSQLI_ASSOC);
			$key = $line['Column_name'];
			*/
			$key = 'ID';
			$where = '`' . $key . '` = "' . $args[0] . '"';
			$i = 1;
		} 
		//--------------------------------------
		$table = mysqli_real_escape_string($GLOBALS['link'], $table);
		//--------------------------------------
		foreach ($args as $key => $value) {
			$args[$key] = strip_tags($args[$key]);
			$args[$key] = mysqli_real_escape_string($GLOBALS['link'], $args[$key]);
		}
		//--------------------------------------
		for (; $i < count($args) ; $i = $i + 2) { 
			$where = $where . ' AND `' . $args[$i] . '` = "' . $args[$i + 1] . '"';
		}
		//--------------------------------------
		$query = 'SELECT max(`' . $row . '`) FROM `' . $table . '` WHERE ' . $where;
		$result = oth_reader_query($query, array());
		//--------------------------------------
		$otvet = array();
		$line = mysqli_fetch_array($result, MYSQLI_ASSOC);
		//--------------------------------------
		return $line['max(`' . $row . '`)'];
		//--------------------------------------
	}
//-------------------------------------------------------------------------------------------------------------------------------
?>
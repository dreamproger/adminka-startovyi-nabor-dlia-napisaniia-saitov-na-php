<?php 
	/*
		Санат Капышев:
			Хранилище функций для main.php
			Дата создания: 27 мая 2015
			Дата последнего изменения: 27 мая 2015

		Apple Cider Studios 2015(C)

	*/
	//--------------------------------------------------------------------
	function start_connection_write() {
		$link = mysqli_connect($GLOBALS['mysql_ip'], $GLOBALS['mysql_writer_user'], $GLOBALS['mysql_writer_password'], $GLOBALS['mysql_db'])  or die('Не удалось соединиться c БД: ' . mysqli_error($link));
		mysqli_set_charset($link, "utf8");
		mysqli_query($link, "SET NAMES 'utf8'");
		mysqli_query($link, "SET SESSION sql_mode = ''");
		return $link;
	}
	//--------------------------------------------------------------------
	function start_connection_read() {
		$link = mysqli_connect($GLOBALS['mysql_ip'], $GLOBALS['mysql_reader_user'], $GLOBALS['mysql_reader_password'], $GLOBALS['mysql_db'])  or die('Не удалось соединиться c БД: ' . mysqli_error($link));
		mysqli_set_charset($link, "utf8");
		mysqli_query($link, "SET NAMES 'utf8'");
		mysqli_query($link, "SET SESSION sql_mode = ''");
		return $link;
	}
	//--------------------------------------------------------------------
	function start_connection_construct() {
		$link = mysqli_connect($GLOBALS['mysql_ip'], $GLOBALS['mysql_construct_user'], $GLOBALS['mysql_construct_password'], $GLOBALS['mysql_db'])  or die('Не удалось соединиться c БД: ' . mysqli_error($link));
		mysqli_set_charset($link, "utf8");
		mysqli_query($link, "SET NAMES 'utf8'");
		mysqli_query($link, "SET SESSION sql_mode = ''");
		return $link;
	}
	//--------------------------------------------------------------------
	function close_connection($link) {
		mysqli_close($link);
	}
	//--------------------------------------------------------------------
	 
	$link = mysqli_connect($GLOBALS['mysql_ip'], $GLOBALS['mysql_reader_user'], $GLOBALS['mysql_reader_password'], $GLOBALS['mysql_db'])  or die('Не удалось соединиться c БД: ' . mysqli_error($link));
	mysqli_set_charset($link, "utf8");
	mysqli_query($link, "SET NAMES 'utf8'");
	/*foreach ($_GET as $key => $value) {
		if ($key == 'HTML') { continue; }
		if ($key == 'json') { continue; }
		$_GET[$key] =  mysqli_real_escape_string($link, $value);
	}
	foreach ($_POST as $key => $value) {
		if ($key == 'HTML') { continue; }
		if ($key == 'json') { continue; }
		$_POST[$key] =  mysqli_real_escape_string($link, $value);
	}*/
	$GLOBALS['link'] = $link;
	//--------------------------------------------------------------------
	require_once('reader_functions.php');
	require_once('writer_functions.php');
	require_once('construct_functions.php');
	//--------------------------------------------------------------------
?>
<?php
//-------------------------------------------------------------------------------------------------------------------------------
	function oth_construct_query($query, $args) {
		/* Все записываемые запросы*/
		//--------------------------------------
		$link =start_connection_construct();
		//--------------------------------------
		foreach ($args as $key => $value) {
			$args[$key] = strip_tags($args[$key]);
			$args[$key] = mysqli_real_escape_string($link, $args[$key]);
		}
		//--------------------------------------
		for ($index = 0; true; $index++) {
			//-------------------------
			$i = stripos($query, '%i');
			$s = stripos($query, '%s');
			//-------------------------
			if (($i == false) & ($s == false)) { break; }
			if ($i == false) { $i = 99999; }
			if ($s == false) { $s = 99999; }
			//-------------------------
			if ($i > $s) {
				$query =  preg_replace('/%s/', '"' . $args[$index] . '"', $query, 1);
			} else {

				if (!is_numeric($args[$index])) { $args[$index] = 0; }
				$query =  preg_replace('/%i/', '"' . $args[$index] . '"', $query, 1);
			}
			//-------------------------
		}
		//--------------------------------------
		if (!mysqli_query($link, $query)) return('Запрос не удался: ' . mysqli_error($link));
		$id = mysqli_insert_id($link);
		close_connection($link);
		return $id;
		//--------------------------------------
	}	
//-------------------------------------------------------------------------------------------------------------------------------
	function createCatRow($table, $rowName){
		//--------------------------------------
		$table = mysqli_real_escape_string($GLOBALS['link'],$GLOBALS['mysql_prefix'] .  $table);
		$rowName = mysqli_real_escape_string($GLOBALS['link'], $rowName);
		//--------------------------------------
		oth_construct_query('ALTER TABLE `' . $table . '` ADD `' . $rowName . '` VARCHAR(500) NOT NULL', array());
		//--------------------------------------
	}
	function deleteCatRow($table, $rowName){
		//--------------------------------------
		$table = mysqli_real_escape_string($GLOBALS['link'],$GLOBALS['mysql_prefix'] . $table);
		$rowName = mysqli_real_escape_string($GLOBALS['link'], $rowName);
		//--------------------------------------
		oth_construct_query('ALTER TABLE `' . $table . '` DROP `' . $rowName . '`;', array());
		//--------------------------------------
	}
//-------------------------------------------------------------------------------------------------------------------------------
?>
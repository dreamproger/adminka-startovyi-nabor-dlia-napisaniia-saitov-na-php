<?php
//-------------------------------------------------------------------------------------------------------------------------------
	function oth_writer_query($query, $args) {
		/* Все записываемые запросы*/
		//--------------------------------------
		$link = start_connection_write();
		//--------------------------------------
		foreach ($args as $key => $value) {
			$args[$key] = strip_tags($args[$key]);
			$args[$key] = mysqli_real_escape_string($link, $args[$key]);
		}
		//--------------------------------------
		for ($index = 0; true; $index++) {
			//-------------------------
			$i = stripos($query, '%i');
			$s = stripos($query, '%s');
			//-------------------------
			if (($i == false) & ($s == false)) { break; }
			if ($i == false) { $i = 99999; }
			if ($s == false) { $s = 99999; }
			//-------------------------
			if ($i > $s) {
				$query =  preg_replace('/%s/', '"' . $args[$index] . '"', $query, 1);
			} else {

				if (!is_numeric($args[$index])) { $args[$index] = 0; }
				$query =  preg_replace('/%i/', '"' . $args[$index] . '"', $query, 1);
			}
			//-------------------------
		}
		//--------------------------------------
		if (!mysqli_query($link, $query)) return('Запрос не удался: ' . mysqli_error($link));
		$id = mysqli_insert_id($link);
		close_connection($link);
		return $id;
		//--------------------------------------
	}	
	function oth_writer_query_special($query) {
		/* Все записываемые запросы*/
		//--------------------------------------
		$link = start_connection_write();
		//--------------------------------------
		if (!mysqli_query($link, $query))  return('Запрос не удался: ' . mysqli_error($link));
		$id = mysqli_insert_id($link);
		close_connection($link);
		return $id;
		//--------------------------------------
	}
//-------------------------------------------------------------------------------------------------------------------------------
	function write_query($query,  ...$args) {
		/* Ставит данные по запросу */

		return (oth_writer_query($query, $args));

	}

//-------------------------------------------------------------------------------------------------------------------------------
	function update_query($table, ...$args){
		/* Обновляет $table согласно $query */
		//--------------------------------------
		$tablekeys = array();
		//--------------------------------------
		$table = $GLOBALS['mysql_prefix'] . $table;
		$table = mysqli_real_escape_string($GLOBALS['link'], $table);
		$result = mysqli_query($GLOBALS['link'], 'SHOW INDEX FROM  `' . $table . '`');
		while ($line = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
			array_push($tablekeys, strtolower($line['Column_name']));
		}
		//--------------------------------------
		foreach ($args as $key => $value) {
			$args[$key] = strip_tags($args[$key]);
			$args[$key] = mysqli_real_escape_string($GLOBALS['link'], $args[$key]);
		}
		//--------------------------------------
		$query = 'UPDATE `' . $table . '` SET ';
		$where = '1 ';
		for ($i=0; $i < count($args) ; $i = $i + 2) { 
			if (!in_array(strtolower($args[$i]), $tablekeys)){
				$query = $query . '`' . $args[$i] . '` = "'	. $args[$i+1] . '", ';
			} else {
				$where = $where . ' AND `' . $args[$i] . '` = "' . $args[$i + 1] . '"';
			}
		}
		//--------------------------------------
		if ($where == '1 ') { return; }
		//--------------------------------------
		$query = rtrim($query, ", ");
		$query = $query . ' WHERE ' . $where;
		//--------------------------------------
		return oth_writer_query($query, array());
		//--------------------------------------
	}
//-------------------------------------------------------------------------------------------------------------------------------
	function update($table, $object){
		/* Обновляет $table согласно $query */
		//--------------------------------------
		$tablekeys = array();
		//--------------------------------------
		$table = $GLOBALS['mysql_prefix'] . $table;
		$table = mysqli_real_escape_string($GLOBALS['link'], $table);
		$result = mysqli_query($GLOBALS['link'], 'SHOW INDEX FROM  `' . $table . '`');
		while ($line = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
			array_push($tablekeys, $line['Column_name']);
		}
		//--------------------------------------
		$object_new = array();
		foreach ($object as $key => $val) {
			$key = mysqli_real_escape_string($GLOBALS['link'], $key);
			$val = mysqli_real_escape_string($GLOBALS['link'], $val);
			//$val = $val;
			$object_new[$key] = $val;
		}
		//--------------------------------------
		$query = 'UPDATE `' . $table . '` SET ';
		$where = '1 ';
		foreach ($object_new as $key => $val) {
			if (!in_array($key, $tablekeys)){
				$query = $query . '`' . $key . '` = "'	. $val . '", ';
			} else {
				$where = $where . ' AND `' . $key . '` = "' . $val . '"';
			} 

		}
		//--------------------------------------
		if ($where == '1 ') { return; }
		//--------------------------------------
		$query = rtrim($query, ", ");
		$query = $query . ' WHERE ' . $where;
		//--------------------------------------
	 		
		return oth_writer_query_special($query, array());
		//--------------------------------------
	}
//-------------------------------------------------------------------------------------------------------------------------------
	function UpdateByPrimaryOnly($table, $object){
		/* Обновляет $table согласно $query */
		//--------------------------------------
		$tablekeys = array();
		//--------------------------------------
		$table = $GLOBALS['mysql_prefix'] . $table;
		$table = mysqli_real_escape_string($GLOBALS['link'], $table);
		$result = mysqli_query($GLOBALS['link'], 'SHOW INDEX FROM  `' . $table . '` WHERE `Key_name` = "PRIMARY"');
		while ($line = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
			array_push($tablekeys, $line['Column_name']);
		}
		//--------------------------------------
		$object_new = array();
		foreach ($object as $key => $val) {
			$key = mysqli_real_escape_string($GLOBALS['link'], $key);
			$val = mysqli_real_escape_string($GLOBALS['link'], $val);
			$object_new[$key] = $val;
		}
		//--------------------------------------
		$query = 'UPDATE `' . $table . '` SET ';
		$where = '1 ';
		foreach ($object_new as $key => $val) {
			if (!in_array($key, $tablekeys)){
				$query = $query . '`' . $key . '` = "'	. $val . '", ';
			} else {
				$where = $where . ' AND `' . $key . '` = "' . $val . '"';
			} 

		}
		//--------------------------------------
		if ($where == '1 ') { return; }
		//--------------------------------------
		$query = rtrim($query, ", ");
		$query = $query . ' WHERE ' . $where;
		//--------------------------------------
	 		
		return oth_writer_query_special($query, array());
		//--------------------------------------
	}
//-------------------------------------------------------------------------------------------------------------------------------
	function insert($table, $object){
		/* Вставляет в $table объект $object */
		//--------------------------------------
		$table = $GLOBALS['mysql_prefix'] . $table;
		//--------------------------------------
		$object_new = array();
		foreach ($object as $key => $val) {
			$key = mysqli_real_escape_string($GLOBALS['link'], $key);
			$val = mysqli_real_escape_string($GLOBALS['link'], $val);
			$object_new[$key] = $val;
		}
		//--------------------------------------
		$columns = '';
		$datas = '';
		foreach ($object_new as $key => $value) {
			$columns = $columns . '`' . $key . '`, ';
			$datas = $datas . '"' . $value . '", ';
		}
		//--------------------------------------
		$columns = rtrim($columns, ", ");
		$datas = rtrim($datas, ", ");
		//--------------------------------------

		$query = 'INSERT INTO `' . $table . '` (' . $columns . ') VALUES (' . $datas . ');';
		//--------------------------------------
		return  oth_writer_query_special($query, array());
		//--------------------------------------
	}
//-------------------------------------------------------------------------------------------------------------------------------
	function remove($table, ...$args){
		/* Удаляет объект из $table согласно $args */
		//--------------------------------------
		$table = $GLOBALS['mysql_prefix'] . $table;
		$i=0;
		$where = '1 ';
		//--------------------------------------
		if (count($args) % 2 == 1) { 
			$key = 'ID';
			$where = '`' . $key . '` = "' . $args[0] . '"';
			$i = 1;
		} 
		//--------------------------------------
		foreach ($args as $key => $value) {
			$args[$key] = strip_tags($args[$key]);
			$args[$key] = mysqli_real_escape_string($GLOBALS['link'], $args[$key]);
		}
		//--------------------------------------
		$query = 'DELETE FROM `' . $table . '` ';
 
		for (; $i < count($args) ; $i = $i + 2) { 
			$where = $where . ' AND `' . $args[$i] . '` = "' . $args[$i + 1] . '"';
		}
		//--------------------------------------
		if ($where == '1 ') { return; }
		//--------------------------------------
		$query = $query . ' WHERE ' . $where;
		//--------------------------------------
		return oth_writer_query($query, array());
		//--------------------------------------
	}
//-------------------------------------------------------------------------------------------------------------------------------
?>
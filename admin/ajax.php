<?php 
 	/* ini_set('display_errors',1);
	ini_set('display_startup_errors',1);
	error_reporting(-1); */
	//----------------------------------------------
	//----------------------------------------------
	include(__DIR__.'/settings.php');
	include(__DIR__.'/datebase.php');
	//----------------------------------------------
	include(__DIR__.'/libs/functions.php');
	include(__DIR__.'/libs/login.php');
	include(__DIR__.'/libs/objects/main.php');
	//----------------------------------------------
	$plugin = escapeshellcmd($_POST['module']);
	//----------------------------------------------
	if ((!$GLOBALS['logged']) & ($plugin != 'login')) { die('Not Logged'); }
	//----------------------------------------------
	if(isset($_POST['g-recaptcha-response'])) {
	    $result = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$GLOBALS['googleCapchaPrivateKey']."&response=".$_POST["g-recaptcha-response"]."&remoteip=".$_SERVER["REMOTE_ADDR"]), TRUE);
	
	    if($result['success'] == 1) {

	    } else {
	        exit(json_encode(array('respons2e' => 0, 'error' => 'Ошибка капчи')));
	    }
	}
	//----------------------------------------------
	$otv = array();
	$response = 0;
	$args = $_POST;
	$query = $args['query'];
	//----------------------------------------------
	(file_exists('ajax/' . $plugin . '.php')) ? include('ajax/' . $plugin . '.php') : $response = '-1';
	//----------------------------------------------
	$otv['response'] = $response;
	echo(json_encode($otv));
	//----------------------------------------------
?>
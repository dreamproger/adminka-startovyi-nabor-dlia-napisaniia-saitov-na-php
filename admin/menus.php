<?php
/* ------------------------------------------------ */

/* ------------------------------------------------ */
    $admins = array('editable', 'rows', 'variants', 'users')
/* ------------------------------------------------ */
?>
<div class="menu_section">

    <ul class="nav side-menu">
      	<li><a><i class="fa fa-home"></i> Главная <span class="fa fa-chevron-down"></span></a>
        	<ul class="nav child_menu">
	      		<li><a href="<?php echo($GLOBALS['url_short']); ?>Главная">Главная страница</a></li>
        	</ul>
    	</li>
    	<li <?php echo(!in_array($GLOBALS['nowpage'], $admins)?'class="active"':"") ?>><a><i class="fa fa-edit"></i> Управление <span class="fa fa-chevron-down"></span></a>
	        <ul <?php echo(!in_array($GLOBALS['nowpage'], $admins)?'style="display: block"':"") ?> class="nav child_menu">
                <?php 
                    $tables = arr_query("SHOW TABLES");
                    foreach ($GLOBALS['items'] as $table2) {
                        if (isset($table2['parent']) || ($table2['parent']??0) != 0) continue;
                        $table = $table2['myname'] ?? $table2['mysqlTabeName'];
                        if ($table=='editable') continue;
                        if ($table=='users') continue;
                        if ($table=='tokens') continue;
                        $name = $table2['name'];
                        echo('<li><a href="'.$GLOBALS['url_short'].$table.'">'.$name.'</a></li>');
                    }
                ?>
                 
            </ul>
        </li>
        <?php if ($GLOBALS['isIMAdmin'] == 1) { ?>
        <li <?php echo(in_array($GLOBALS['nowpage'], $admins)?'class="active"':"") ?>><a><i class="fa fa-edit"></i> Конструирование <span class="fa fa-chevron-down"></span></a>
             <ul class="nav child_menu" <?php echo(in_array($GLOBALS['nowpage'], $admins)?'style="display: block"':"") ?>><?php 
                $tables = arr_query("SHOW TABLES");
                foreach ($GLOBALS['items'] as $table2) {
                    if (!isset($table2['ID'])) continue;
                    $table = $table2['ID'];
                    if ($table=='editable') continue;
                    $name = $table2['name'];
                    echo('<li><a href="'.$GLOBALS['url_short'].'editable/просмотр/'.$table.'">'.$name.'</a></li>');
                }
            ?>
                <li><a href="%base%users"> Администраторы </a></li>
                <li><a href="%base%editable/создать/"> Новый объект </a></li>
            </ul>
        </li>
        <?php } ?>
	    
    </ul>
</div>
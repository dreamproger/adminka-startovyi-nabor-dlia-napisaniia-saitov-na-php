<?php
 
	/* ---------------------------------------------------------------------------- */
		ini_set('display_errors',1);
		ini_set('display_startup_errors',1);
		error_reporting(-1);
	/* ---------------------------------------------------------------------------- */
		include(__DIR__.'/settings.php');
		include(__DIR__.'/datebase.php');
	/* ---------------------------------------------------------------------------- */
		include(__DIR__.'/libs/functions.php');
		include(__DIR__.'/libs/login.php');
		include(__DIR__.'/libs/objects/main.php');
		include(__DIR__.'/libs/showpage.php');
	/* ---------------------------------------------------------------------------- */
?>
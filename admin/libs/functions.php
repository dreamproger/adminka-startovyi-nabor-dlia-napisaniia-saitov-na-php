<?php
	
	/* -------------------------------------------------------------------------- */
	function page($page, $pages){
		$CurrentFolderIndex = 1;
		ob_start ();
		if (file_exists('pages/' . $page . '.php')) { include('pages/' . $page . '.php'); } else { $c = 'Ошибка 404: Файл не найден'; }
		$c = ob_get_contents();
		ob_end_clean ();
		return $c;  
	}
	/* -------------------------------------------------------------------------- */
	function get_template($page) { 
		// Получить шаблон
		$e = 1; 
		ob_start ();
		include('template/' . $page . '.php');
		$c = ob_get_contents();
		ob_end_clean ();
		return $c;  
	};
	/* -------------------------------------------------------------------------- */
	function gopage($page){
		header( 'Location: ' . $GLOBALS['url_short']  . $page, true, 302 );
		die('');
	}
	/* -------------------------------------------------------------------------- */
	function get_menus() { 
		// Получить шаблон
		$e = 1; 
		ob_start ();
		include('menus.php');
		$c = ob_get_contents();
		ob_end_clean ();
		return $c;  
	};
	/* -------------------------------------------------------------------------- */
	function TriesCount($type, $exp = 10){
		return true;
	}
	/* -------------------------------------------------------------------------- */
	function AuthMe($user){
		$token = array('UID' => $user['ID'], 'token' => md5(time() . '-' . $GLOBALS['mysql_prefix']  . ' - ' . $user['ID'] . ' - ' . $user['email']) );
		$id = insert('tokens', $token);
		SetCookieVal($GLOBALS['mysql_prefix'] . 'token', $token['token']);
		write_query("UPDATE `tokens` SET `LatestUsed`= NOW() WHERE `ID` = %i", $id);
	}
	function dislog(){
		$token = get('tokens', 'token', $_COOKIE[$GLOBALS['mysql_prefix'] . 'token']);
		remove('tokens', $token['ID']);
		SetCookieVal($GLOBALS['mysql_prefix'], null);
		$_SESSION['logged'] = false;
		$_SESSION['user'] = null;
	}
	function recheckToken(){
		$token = get('tokens', 'token', $_COOKIE[$GLOBALS['mysql_prefix'] . 'token']);
		if ($token == null) return;
		write_query("UPDATE `tokens` SET `LatestUsed`= NOW() WHERE `ID` = %i", $token['ID']);
	}
	/* -------------------------------------------------------------------------- */
	function SetCookieVal($key, $val){
		if ($GLOBALS['url_short'] != '/') {
			$path =  '/' . urlencode (substr($GLOBALS['url_short'], 1, -1)) . '/';
		} else {
			$path =  '/';
		}
		setcookie($key, $val, 2000000000, $path , $_SERVER['SERVER_NAME']);

	}
	/* -------------------------------------------------------------------------- */
	function GetUsersById(){
		$users_temp = arr('users');
		$users = array();
		foreach ($users_temp as $key => $user) {
			$users[$user['ID']] = $user;
		}
		$users[0] = array('Name' => 'Система', 'SubName' => '', 'LastName' => '', 'email' => '', 'ID' => 0);
		return $users;
	}
	/* -------------------------------------------------------------------------- */
	function GetUserFio($user){
		if (is_null($user)) return 'Пользователь удален';
		return $user['SubName'] . ' ' . $user['Name'] . ' ' . $user['LastName'];
	}
	/* -------------------------------------------------------------------------- */
	function GetUserAvatar($user){
		return '%base%users/mini/' . $user['ID'];
	}
	/* -------------------------------------------------------------------------- */
	function GetModulesInfo(){
		$modules = array();
		$plugins = GetFolders($GLOBALS['folder'] . '/modules');
		foreach ($plugins as $plugin) {
			$module = array();
			include($plugin . '/info.php');
			$modules[$module['name']] = $module;
		}
		return $modules;
	}
	/* -------------------------------------------------------------------------- */
	function GetFolders($path){
		$folders = array();
		$results = scandir($path);

		foreach ($results as $result) {
		    if ($result === '.' or $result === '..') continue;
		    if ($result == 'editor') continue;
		    if (is_dir($path . '/' . $result)) {
		        array_push($folders, $path . '/' . $result);
		    }
		}
		return $folders;
	}
	function GetFoldersNames($path){
		$folders = array();
		$results = scandir($path);

		foreach ($results as $result) {
		    if ($result === '.' or $result === '..') continue;
		    if ($result == 'editor') continue;
		    if (is_dir($path . '/' . $result)) {
		        array_push($folders,  $result);
		    }
		}
		return $folders;
	}
	/* -------------------------------------------------------------------------- */
	function ConvertDataToRussian($data){
		$MonthNames=array("Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря");
		$date = new DateTime($data);
		return ($date->format('d ') . $MonthNames[$date->format('m') - 1] . $date->format(' Y'));
	}
	function ConvertDataToTime($data){
		$date = new DateTime($data);
		return ($date->format('H:i'));
	}
	/* -------------------------------------------------------------------------- */
	function ValidateURL($url){
		return str_replace(' ', '_', $url);
	}
	function DeValidateURL($url){
		return str_replace('_', ' ', $url);
	}
	/* -------------------------------------------------------------------------- */
	function getExtension($filename) {
	    $tmp1 = explode(".", $filename);
	    return (end($tmp1));
	  }
	/* -------------------------------------------------------------------------- */
	function check_img($file) {

	   $x = getimagesize($file);

	   switch ($x['mime']) {
	      case "image/gif":
	         return 1;
	         break;
	      case "image/jpeg":
	         return 1;
	         break;
	      case "image/png":
	         return 1;
	         break;
	      default:
	         return 0;
	         break;
	   }

	   return 0;
	}
	/* -------------------------------------------------------------------------- */
	function addhttp($url) {
	    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
	        $url = "http://" . $url;
	    }
	    return $url;
	}
	/* -------------------------------------------------------------------------- */
	function SafeHTML($dirty_html){
		require_once 'HTMLPurifer/HTMLPurifier.auto.php';
		$config = HTMLPurifier_Config::createDefault();
		//---------------------------------------------
		$config->set('Cache.DefinitionImpl', null);
		$config->set('HTML.SafeIframe', true);
		$config->set('URI.SafeIframeRegexp', '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%');
		//---------------------------------------------
		$def = $config->getHTMLDefinition(true);
		$def->addAttribute('a', 'target', 'Enum#_blank');
		$def->addAttribute('font', 'color', new HTMLPurifier_AttrDef_HTML_Color());
		$def->addAttribute('font', 'size', 'Number');
		$def->addElement('audio', 'Block', 'Optional: (source, Flow) | (Flow, source) | Flow', 'Common', array(
		    'src' => 'URI',
		    'type' => 'Text',
		    'width' => 'Length',
		    'height' => 'Length',
		    'poster' => 'URI',
		    'preload' => 'Enum#auto,metadata,none',
		    'controls' => 'Bool',
		  ));
		//---------------------------------------------

		//---------------------------------------------
		$purifier = new HTMLPurifier($config);
		return ($purifier->purify($dirty_html));
	}
	function SafeHTMLStyles($dirty_html){
		require_once 'HTMLPurifer/HTMLPurifier.auto.php';
		$config = HTMLPurifier_Config::createDefault();
		//---------------------------------------------
		$config->set('Cache.DefinitionImpl', null);
		$config->set('HTML.SafeIframe', false);
		//---------------------------------------------
		$config->set('URI.SafeIframeRegexp', '%^(https?:)?//(www\.youtube(?:-nocookie)?\.com/embed/|player\.vimeo\.com/video/)%');
		//---------------------------------------------
		$def = $config->getHTMLDefinition(true);
		$def->addAttribute('a', 'target', 'Enum#_blank');
		$def->addAttribute('*', 'style', 'Text');
		$def->addAttribute('font', 'color', new HTMLPurifier_AttrDef_HTML_Color());
		$def->addAttribute('font', 'size', 'Number');
		
		$def->addAttribute('div', 'data-href', new HTMLPurifier_AttrDef_Text());
		$def->addAttribute('div', 'data-blockid', new HTMLPurifier_AttrDef_Text());
		$def->addAttribute('div', 'data-connectedpage', new HTMLPurifier_AttrDef_Text());
		$def->addAttribute('div', 'data-add1', new HTMLPurifier_AttrDef_Text());
		$def->addAttribute('div', 'data-add2', new HTMLPurifier_AttrDef_Text());
		$def->addAttribute('div', 'data-add3', new HTMLPurifier_AttrDef_Text());
		$def->addAttribute('iframe', 'src', new HTMLPurifier_AttrDef_Text());
		$def->addElement('audio', 'Block', 'Optional: (source, Flow) | (Flow, source) | Flow', 'Common', array(
		    'src' => 'URI',
		    'type' => 'Text',
		    'width' => 'Length',
		    'height' => 'Length',
		    'poster' => 'URI',
		    'preload' => 'Enum#auto,metadata,none',
		    'controls' => 'Bool',
		  ));
		$def->addElement('iframe', 'Block', 'Optional: (source, Flow) | (Flow, source) | Flow', 'Common', array(
		    'src' => 'URI',
		  ));
		$def->addElement('input', 'Block', 'Flow', 'Common', array("type" => 'Text', 'data-id' => 'Text', 'placeholder' => 'Text', 'style' => "Text", 'data-landpage' => 'Text', 'data-send' => 'Text'));
		$def->addElement('select', 'Block', 'Flow', 'Common', array("type" => 'Text', 'data-id' => 'Text', 'placeholder' => 'Text', 'style' => "Text", 'data-landpage' => 'Text', 'data-send' => 'Text'));
		$def->addElement('textarea', 'Block', 'Flow', 'Common', array("type" => 'Text', 'data-id' => 'Text', 'placeholder' => 'Text', 'style' => "Text", 'data-landpage' => 'Text', 'data-send' => 'Text'));
		$def->addElement('option', 'Block', 'Flow', 'Common', array("type" => 'Text', 'data-id' => 'Text', 'placeholder' => 'Text', 'style' => "Text", 'data-landpage' => 'Text', 'data-send' => 'Text'));
		$def->addAttribute('div', 'data-thanks', new HTMLPurifier_AttrDef_Text() );
		
		//---------------------------------------------
		$css = $config->getCSSDefinition();
		$border_radius =
		$css->info['border-top-left-radius'] =
		$css->info['border-top-right-radius'] =
		$css->info['border-bottom-left-radius'] =
		$css->info['border-bottom-right-radius'] = new HTMLPurifier_AttrDef_CSS_Composite(array(
		 new HTMLPurifier_AttrDef_CSS_Length('0'),
		 new HTMLPurifier_AttrDef_CSS_Percentage(true)
		));
		$css->info['border-radius'] = new HTMLPurifier_AttrDef_CSS_Multiple($border_radius);
		$css->info['border-width'] = new HTMLPurifier_AttrDef_CSS_Length();
		$css->info['display'] = new  HTMLPurifier_AttrDef_Text();
		$css->info['border-size'] = new HTMLPurifier_AttrDef_CSS_Composite(array( new HTMLPurifier_AttrDef_CSS_Length(), new HTMLPurifier_AttrDef_CSS_Percentage(true) ));
		$css->info['min-height'] = new HTMLPurifier_AttrDef_CSS_Composite(array( new HTMLPurifier_AttrDef_CSS_Length(), new HTMLPurifier_AttrDef_CSS_Percentage(true) ));
		$css->info['background-size'] = new HTMLPurifier_AttrDef_CSS_Composite(array( new HTMLPurifier_AttrDef_CSS_Length(), new HTMLPurifier_AttrDef_CSS_Percentage(true), new HTMLPurifier_AttrDef_Text() ));
		$css->info['text-shadow'] = new HTMLPurifier_AttrDef_CSS_Composite(array( new HTMLPurifier_AttrDef_CSS_Length(), new HTMLPurifier_AttrDef_CSS_Percentage(true), new HTMLPurifier_AttrDef_Text() ));
		//---------------------------------------------
		$purifier = new HTMLPurifier($config);
		return ($purifier->purify($dirty_html));
	}
	/* -------------------------------------------------------------------------- */
	function update_HTML($table, $object, $HTMLRow){
		/* Обновляет $table согласно $query */
		//--------------------------------------
		$tablekeys = array();
		//--------------------------------------
		$table = $GLOBALS['mysql_prefix'] . $table;
		$table = mysqli_real_escape_string($GLOBALS['link'], $table);
		$result = mysqli_query($GLOBALS['link'], 'SHOW INDEX FROM  `' . $table . '`');
		while ($line = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
			array_push($tablekeys, $line['Column_name']);
		}
		//--------------------------------------
		$object_new = array();
		foreach ($object as $key => $val) {
			if ($key == $HTMLRow){
				$val = mysqli_real_escape_string($GLOBALS['link'],SafeHTML($val));
			} else {
				$key = mysqli_real_escape_string($GLOBALS['link'], $key);
				$val = mysqli_real_escape_string($GLOBALS['link'], $val);
			 }
			$object_new[$key] = $val;
		}
		//--------------------------------------
		$query = 'UPDATE `' . $table . '` SET ';
		$where = '1 ';
		foreach ($object_new as $key => $val) {
			if (!in_array($key, $tablekeys)){
				$query = $query . '`' . $key . '` = "'	. $val . '", ';
			} else {
				$where = $where . ' AND `' . $key . '` = "' . $val . '"';
			} 

		}
		//--------------------------------------
		if ($where == '1 ') { return; }
		//--------------------------------------
		$query = rtrim($query, ", ");
		$query = $query . ' WHERE ' . $where;
		//--------------------------------------
		return oth_writer_query_special($query, array());
		//--------------------------------------
	}
	/* -------------------------------------------------------------------------- */
	function update_HTML2row($table, $object, $HTMLRow, $HTMLRow2){
		/* Обновляет $table согласно $query */
		//--------------------------------------
		$tablekeys = array();
		//--------------------------------------
		$table = $GLOBALS['mysql_prefix'] . $table;
		$table = mysqli_real_escape_string($GLOBALS['link'], $table);
		$result = mysqli_query($GLOBALS['link'], 'SHOW INDEX FROM  `' . $table . '`');
		while ($line = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
			array_push($tablekeys, $line['Column_name']);
		}
		//--------------------------------------
		$object_new = array();
		foreach ($object as $key => $val) {
			if ($key == $HTMLRow){
				$val = mysqli_real_escape_string($GLOBALS['link'],SafeHTMLStyles($val));
			} else if ($key == $HTMLRow2) {
				$val = mysqli_real_escape_string($GLOBALS['link'], $val);
			} else {
				$key = mysqli_real_escape_string($GLOBALS['link'], $key);
				$val = mysqli_real_escape_string($GLOBALS['link'], $val);
			 }
			$object_new[$key] = $val;
		}
		//--------------------------------------
		$query = 'UPDATE `' . $table . '` SET ';
		$where = '1 ';
		foreach ($object_new as $key => $val) {
			if (!in_array($key, $tablekeys)){
				$query = $query . '`' . $key . '` = "'	. $val . '", ';
			} else {
				$where = $where . ' AND `' . $key . '` = "' . $val . '"';
			} 

		}
		//--------------------------------------
		if ($where == '1 ') { return; }
		//--------------------------------------
		$query = rtrim($query, ", ");
		$query = $query . ' WHERE ' . $where;
		//--------------------------------------
		return oth_writer_query_special($query, array());
		//--------------------------------------
	}
	/* -------------------------------------------------------------------------- */
	function startsWith($haystack, $needle) {
	    return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
	}
	function endsWith($haystack, $needle) {
	    return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== FALSE);
	}

	/* -------------------------------------------------------------------------- */
	function CheckIsExists($pages, $index){
		if (!isset($pages[$index])) {
			return false;
		} else {
			if ($pages[$index] != '') {
				return true;
			} else {
				return false;
			}
		}
	}
	/* -------------------------------------------------------------------------- */
	function CheckIsFolderPart($page, $pages, $current){
		if ($page['Plugin'] != 'folder') return false;
		$end = end($pages);
		if ($end == '') $end = prev($pages);
		if ($end == $current) return false;
		return true;
	}
	/* -------------------------------------------------------------------------- */
	function NormalizeCase($text) {
	    return str_replace('_', ' ',mb_strtoupper(mb_substr($text, 0, 1)) . mb_strtolower(mb_substr($text, 1)));
	}
	/* -------------------------------------------------------------------------- */
	function UpdateEditTimeSinglePage($id){
		$page = array( 'ID' => $id, 'EditTime' => date('Y-m-d H:i:s'));
		update('pages', $page);
	}
	function UpdateEditTimeUnion($name){
		$pagesHasThisInUnion = arr_query('SELECT * FROM `' . $GLOBALS['mysql_prefix'] . 'pages` WHERE `UnionWith` like "%' . CheckString($name) . '%"');

		foreach ($pagesHasThisInUnion as $page) {
			UpdateEditTimeSinglePage($page['ID']);
		}
	}
	function UpdateEditTimePage($id){

		$nextID = $id;
		do{
			$pageSub = get('pages', $nextID);
			UpdateEditTimeSinglePage($pageSub['ID']);
			UpdateEditTimeUnion($pageSub['name']);
			$nextID = $pageSub['Parent'];
		} while ($nextID != 0);
	}
	function UpdateAllPages(){
		oth_writer_query( 'UPDATE `' . $GLOBALS['mysql_prefix'] . 'pages` SET `EditTime` = NOW() WHERE 1', array());
	}
	/* -------------------------------------------------------------------------- */
	function getUplinkAll(){
		$page = get_query('SELECT COUNT(*) FROM `' . $GLOBALS['mysql_prefix'] . 'uplink` WHERE 1');
		return $page['COUNT(*)'];
	}
	function getUplinkCount(){
		$page = get_query('SELECT COUNT(*) FROM `' . $GLOBALS['mysql_prefix'] . 'uplink` WHERE `Readed` = 0');
		return $page['COUNT(*)'];
	}
	/* -------------------------------------------------------------------------- */
	function DoError($err){
		$otv = array('err' => $err, 'response' => 0);
		die(json_encode($otv));
	}
	/* -------------------------------------------------------------------------- */
	function GetPageURL($pageId){
		$page = get('pages', $pageId);
		if ($page['Parent'] != 0) return   GetPageURL($page['Parent'])  . ValidateURL($page['name'])  . '/' ; else return  $page['name'] . '/' ;
	}
	/* -------------------------------------------------------------------------- */
	function g($e){
		$block = 'index';
		
		if (!isset($GLOBALS['settings'])) $GLOBALS['settings'] = array();
		if (!isset($GLOBALS['settings'][$block])) {
			$GLOBALS['settings'][$block] = array();
			$a = arr($block);
			foreach ($a as $b) switch ($b['Type']) {
				case 'MultiLine':
					 $GLOBALS['settings'][$block][$b['ID']] = str_replace("\n", '<br>', $b['Value']);
					break;
				case 'Image':
					$GLOBALS['settings'][$block][$b['ID']] = explode(',', $b['Value'])[0].'?cache='.strtotime($b['LastUpdate']);
					break;
				default:
					$GLOBALS['settings'][$block][$b['ID']] = $b['Value'];
					break;
			}
			//foreach ($a as $b) if ($b['Type'] == 'MultiLine') $GLOBALS['settings'][$block][$b['ID']] = str_replace("\n", '<br>', $b['Value']); else 
		}
		return $GLOBALS['settings'][$block][$e] ?? '';
	}
	/* -------------------------------------------------------------------------- */
	function changeDB(){
		if (isset($GLOBALS['oldDB'])) return;
		$GLOBALS['oldDB'] = $GLOBALS['mysql_db'];
		$GLOBALS['mysql_db'] = $GLOBALS['mysql_db2'];
		$link = mysqli_connect($GLOBALS['mysql_ip'], $GLOBALS['mysql_reader_user'], $GLOBALS['mysql_reader_password'], $GLOBALS['mysql_db'])  or die('Не удалось соединиться c БД: ' . mysqli_error($link));
		mysqli_set_charset($link, "utf8");
		mysqli_query($link, "SET NAMES 'utf8'");
	 	$GLOBALS['link'] = $link;
	}
	function restoreDB(){
		 
		$GLOBALS['mysql_db'] = $GLOBALS['oldDB'] ;
		$link = mysqli_connect($GLOBALS['mysql_ip'], $GLOBALS['mysql_reader_user'], $GLOBALS['mysql_reader_password'], $GLOBALS['mysql_db'])  or die('Не удалось соединиться c БД: ' . mysqli_error($link));
		mysqli_set_charset($link, "utf8");
		mysqli_query($link, "SET NAMES 'utf8'");
	 	$GLOBALS['link'] = $link;
	}
	/* -------------------------------------------------------------------------- */

	/* -------------------------------------------------------------------------- */
?>
//--------------------------------------------------------------------------------
	function DoQuery(page, query, data, callback){
		data['module'] = page;
		data['query'] = query;
		$.post(fold + 'ajax.php', data, function(data){
			callback( $.parseJSON(data));
		});
	}
//--------------------------------------------------------------------------------
	function DoFileQuery(page, query, data, going, error, success, fileinput){
		var fd = new FormData();
		//------------------------------------------------------------------------
		$.each(data, function(key, value) {
			fd.append(key, value);
		});
		//------------------------------------------------------------------------
		fd.append('module', page);
		fd.append('query', query);
		//------------------------------------------------------------------------
		fd.append('file', fileinput.files[0]);
		//------------------------------------------------------------------------
		var ajax = new XMLHttpRequest();
		ajax.upload.addEventListener("progress", function(event){ var percent = (event.loaded / event.total) * 100; going(Math.trunc(percent)); }, false);
		ajax.addEventListener("load", function(event){ 
			try{
			var json = $.parseJSON(event.target.responseText); } catch (err) { error("Слишком большой файл"); }
			success(json); 			
		} , false);
		ajax.addEventListener("error", function(event){ error(event.target.statusText); }, false);
		ajax.addEventListener("abort", function(event){ error(event.target.statusText); }, false);
		ajax.open("POST", fold + 'ajax.php');
		ajax.send(fd);
		//------------------------------------------------------------------------
		return ajax;
	}
//--------------------------------------------------------------------------------
	function DoBigStringQuery(page, query, data, going, error, success){

		var fd = new FormData();
		//------------------------------------------------------------------------
		$.each(data, function(key, value) {
			fd.append(key, value);
		});
		fd.append('module', page);
		fd.append('query', query);
		//------------------------------------------------------------------------
		var ajax = new XMLHttpRequest();
		ajax.upload.addEventListener("progress", function(event){ var percent = (event.loaded / event.total) * 100; going(Math.trunc(percent)); }, false);
		ajax.addEventListener("load", function(event){ 
			try{
			var json = $.parseJSON(event.target.responseText); } catch (err) { error("Слишком большой файл"); }
			success(json); 			
		} , false);
		ajax.addEventListener("error", function(event){ error(event.target.statusText); }, false);
		ajax.addEventListener("abort", function(event){ error(event.target.statusText); }, false);
		ajax.open("POST", fold + 'ajax.php');
		ajax.send(fd);
		//------------------------------------------------------------------------
		return ajax;
	}
//--------------------------------------------------------------------------------
function DoSingleFileQuery(page, query, data, going, error, success, file){
		var fd = new FormData();
		//------------------------------------------------------------------------
		$.each(data, function(key, value) {
			fd.append(key, value);
		});
		//------------------------------------------------------------------------
		fd.append('module', page);
		fd.append('query', query);
		//------------------------------------------------------------------------
		fd.append('file', file);
		//------------------------------------------------------------------------
		var ajax = new XMLHttpRequest();
		ajax.upload.addEventListener("progress", function(event){ var percent = (event.loaded / event.total) * 100; going(Math.trunc(percent)); }, false);
		ajax.addEventListener("load", function(event){ 
			try{
			var json = $.parseJSON(event.target.responseText); } catch (err) { error("Слишком большой файл"); }
			success(json); 			
		} , false);
		ajax.addEventListener("error", function(event){ error(event.target.statusText); }, false);
		ajax.addEventListener("abort", function(event){ error(event.target.statusText); }, false);
		ajax.open("POST", fold + 'ajax.php');
		ajax.send(fd);
		//------------------------------------------------------------------------
		return ajax;
	}
//--------------------------------------------------------------------------------
function replaceAll(find, replace, str) {
  return str.replace(new RegExp(find, 'g'), replace);
}
 
//--------------------------------------------------------------------------------
	
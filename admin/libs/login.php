<?php
	/* ------------------------------------------------------------------- */
	$logged = false;
	$GLOBALS['isIMAdmin'] = 0;
	if (isset($_SESSION['logged'])) if ($_SESSION['logged'] == true) { $logged = true; $user = $_SESSION['user']; }
	/* ------------------------------------------------------------------- */
	if ($logged == false){
		if (isset($_COOKIE[$GLOBALS['mysql_prefix'] . 'token'])){
			$token = get('tokens', 'token', $_COOKIE[$GLOBALS['mysql_prefix'] . 'token']);
			if (!is_null($token)){
				$user = get('users', 'ID', $token['UID']);
				if (!is_null($user)){
					$logged = true;
					$user = $user;
					$GLOBALS['isIMAdmin']= ($user['Type'] == 0) ? 1 : 0;
				}
			}
		}
	}
	/* ------------------------------------------------------------------- */
	$GLOBALS['logged'] = $logged;
	$GLOBALS['user'] = $user;
	/* ------------------------------------------------------------------- */
?>
<?php 
		$GLOBALS['items'] = array();
	//------------------------------------------------------------------
		$name = 'index';
		$GLOBALS[$name] = array(
			'items' => array(
				'ID' => array('Type' => 'Integer', 'ReadOnly' => 1, 'Desc' => '#', 'inList' => 1),
				'Desc' => array('Type' => 'String', 'ReadOnly' => $GLOBALS['isIMAdmin']!=1, 'Desc' => 'Описание', 'PlaceHolder' => 'Название', 'inList' => 1),
				'Value' => array('Type' => 'Special1', 'ReadOnly' => 0, 'MaxLength' => 500, 'Desc' => 'Значение', 'inList' => 1),
				'Type' => array('Type' => 'List', 'ReadOnly' => 0, 'Desc' => 'Тип', 'inList' => $GLOBALS['isIMAdmin']!=1,	'PlaceHolder' => '', 'Items' => array('Text' => 'Текст', 'MultiLine' => 'Строка', 'HTML' => 'HTML')),
			),
			'name' => 'Статичная информация',
			'nameGen' => 'настройек',
			'mysqlTabeName' => $name,
			'isObject' => 1,
			'isAddable' => $GLOBALS['isIMAdmin']==1
		);
		$GLOBALS[$name]['linkOne'] = $GLOBALS['url_short'].$GLOBALS[$name]['mysqlTabeName'].'/редактировать/%id%';
		$GLOBALS[$name]['linkOneR'] = $GLOBALS['url_short'].$GLOBALS[$name]['mysqlTabeName'].'/просмотр/%id%';
		$GLOBALS[$name]['linkCreate'] = $GLOBALS['url_short'].$GLOBALS[$name]['mysqlTabeName'].'/создать';
		$GLOBALS[$name]['linkFolder'] = $GLOBALS['url_short'].$GLOBALS[$name]['mysqlTabeName'];
		array_push($GLOBALS['items'], $GLOBALS[$name]);
	//------------------------------------------------------------------
		$editable = arr('editable');
		foreach ($editable as $e) {
			$name = 'r'.$e['mysqlName'];
			$rows = arr_order('rows', 'ID', 'Parent', $e['ID']); $items2 = array('ID' => array('Type' => 'Integer', 'ReadOnly' => 1, 'Desc' => '#', 'inList' => 1));
			foreach ($rows as $row) {
			 
				$item = array("ReadOnly" => $row['ReadOnly'], "Desc" => $row['Name'], 'inList' => $row['InList']??0);
				if ($row['Type'] === null) continue;
				switch ($row['Type']) {
					case 0:
						// Число
						$item['Type'] = 'Integer';
						break;
					case 1:
						// Строка
						$item['Type'] = 'String';
						break;
					case 2:
						// Многострочная строка
						$item['Type'] = 'MultiLine';
						break;
					case 3:
						// Дата
						$item['Type'] = 'Date';
						break;
					case 4:
						// Чекбокс
						$item['Type'] = 'CheckBox';
						break;
					case 5:
						// Перечисление
						$vrs = array();
						$variants = arr('variants', "Parent", $row['ID']); 
						foreach ($variants as $vrnt) $vrs[$vrnt['ID']] = $vrnt['Name'];
						$item['Type'] = 'List';
						$item['Items'] = $vrs;
						break;
					case 6:
						// Перечисление объектов
						$main = get('editable', $row['Obj_Tp']);
						if (!is_null($main)){
							$item['Table']= array("Name" =>'r'.$main['mysqlName'], "MainProper" => $row['Obj_Pl']);
							$item['Type'] = 'ObjectList';
						} else {
							continue;
						}
						break;
					case 7:
						// HTML
						$item['Type'] = 'HTML';
						break;
					case 8:
						// Картинка
						$item['Type'] = 'Image';
						$item['maxwidth'] = $row['Img_maxwidth'];
						$item['maxheight'] = $row['Img_maxheight'];
						
						if ($row['Img_type'] == 1)
							$type = 'png';
						else if ($row['Img_type'] == 2)
							$type = 'gif';
						else 
							$type = 'jpg';
						
						$item['path'] = '/uploads/'.$e['mysqlName'].'/%id%/'.$row['mysqlName'].'.'.$type;
						 
						$item['createFolder'] = dirname($item['path']);
						break;
					case 9:
						// Карта
						$item['Type'] = 'Geo';
						break;
					case 10:
						// Файл
						$item['Type'] = 'File';
					 
						
						$item['path'] = '/uploads/'.$e['mysqlName'].'/%id%/'.$row['mysqlName'].'/%file%';
						$item['createFolder'] = dirname($item['path']);
						break;
					default:
						continue;
				}
				$items2[$row['mysqlName']] = $item;
			}
			$items2['Date'] = array('Type' => 'Date',  	'ReadOnly' => 1, 'Desc' => 'Дата создания',	'inList' => 1,	'PlaceHolder' => '');
			$GLOBALS[$name] = array(
				// 0 -'Число', 1- "Строка", 2 - "Многострочная строка", 3 - "Дата", 4 - "ЧекБокс", 5 - "Перечисление", 6 - "Перечисление объектов", 7 - "HTML", 8 - "Картинка", 9 - "Карта"
				'items' => $items2,
				'name' => $e['Name'],
				'nameGen' => $e['NameGen'],
				'mysqlTabeName' => $e['mysqlName'],
				'myname' => $name,
				'isObject' => 1,
				'isAddable' => $e['isAddable']==1?0:1,
				'ID' => $e['ID']
			);
			if ($e['oParent'] != 0) {
				$p = get('editable', $e['oParent']);
				if (!is_null($p)){
					$GLOBALS[$name]['parent'] = 'r'.$p['mysqlName'];
					$GLOBALS[$name]['items']['Parent'] = array('Type' => 'Integer', 'ReadOnly' => 1, 'Desc' => '# Родителя', 'inList' => 0);
				}
			}
			$sub = arr("editable", "oParent", $e['ID']);
			foreach ($sub as $id => $value) {
				$GLOBALS[$name]['additionalPageButton'.($id+1)] = array('url' => $GLOBALS['url_short'].'r'.$value['mysqlName'].'/%id%', 'name' => $value['Name']);
			}
			$GLOBALS[$name]['linkOne'] = $GLOBALS['url_short'].$GLOBALS[$name]['myname'].'/редактировать/%id%';
			$GLOBALS[$name]['linkOneR'] = $GLOBALS['url_short'].$GLOBALS[$name]['myname'].'/просмотр/%id%';
			$GLOBALS[$name]['linkCreate'] = $GLOBALS['url_short'].$GLOBALS[$name]['myname'].'/создать';
			$GLOBALS[$name]['linkSort'] = $GLOBALS['url_short'].$GLOBALS[$name]['myname'].'/сортировка';
			$GLOBALS[$name]['linkFolder'] = $GLOBALS['url_short'].$GLOBALS[$name]['myname'];

			array_push($GLOBALS['items'], $GLOBALS[$name]);

		}
		 
	//------------------------------------------------------------------
		$name = 'editable';
		$GLOBALS[$name] = array(
			'items' => array(
				'ID' => array('Type' => 'Integer', 'ReadOnly' => 1, 'Desc' => '#', 'inList' => 1),
				'Name' => array('Type' => 'String', 'ReadOnly' => 0, 'Desc' => 'Название', 'inList' => 1,	'PlaceHolder' => ''),
				'NameGen' => array('Type' => 'String', 'ReadOnly' => 0, 'Desc' => 'Название в родительном падеже', 'inList' => 0,	'PlaceHolder' => ''),
				'mysqlName' => array('Type' => 'String', 'ReadOnly' => 0, 'Desc' => 'Название в MySQL', 'inList' => 1, 	'PlaceHolder' => '', 'PostText' => 'На английском, до 20 символов'),
				'isAddable' => array('Type' => 'List', 'ReadOnly' => 0, 'Desc' => 'Тип', 'inList' => 1,	'PlaceHolder' => '', 'Items' => array('Можно добавлять строки', 'Нельзя добавлять строки')),
				'oParent' => array('Type' => 'ObjectList', 'ReadOnly' => 0, 'Desc' => 'Родительский элемент', 'inList' => 1, 	'PlaceHolder' => '', 'PostText' => 'На английском, до 20 символов','Table' => array('Name' => 'editable', 'MainProper' => 'Name'))
			),
			'name' => 'Объекты',
			'nameGen' => 'объектов',
			'mysqlTabeName' => $name,
			'isObject' => 1,
			'isAddable' => true,
			'additionalPageButton' => array('url' => $GLOBALS['url_short'].'rows/%id%', 'name' => 'Столбцы'),
			'mainProper' => 'Name',
			'predName' => 'Объект "%name%"'
		);
		$GLOBALS[$name]['linkOne'] = $GLOBALS['url_short'].$GLOBALS[$name]['mysqlTabeName'].'/редактировать/%id%';
		$GLOBALS[$name]['linkOneR'] = $GLOBALS['url_short'].$GLOBALS[$name]['mysqlTabeName'].'/просмотр/%id%';
		$GLOBALS[$name]['linkCreate'] = $GLOBALS['url_short'].$GLOBALS[$name]['mysqlTabeName'].'/создать';
		$GLOBALS[$name]['linkFolder'] = $GLOBALS['url_short'].$GLOBALS[$name]['mysqlTabeName'];
		array_push($GLOBALS['items'], $GLOBALS[$name]);
	//------------------------------------------------------------------
		$name = 'rows';
		$GLOBALS[$name] = array(
			'items' => array(
				'ID' => array('Type' => 'Integer', 'ReadOnly' => 1, 'Desc' => '#', 'inList' => 1),
				'Name' => array('Type' => 'String', 'ReadOnly' => 0, 'Desc' => 'Название', 'inList' => 1,	'PlaceHolder' => ''),
				'mysqlName' => array('Type' => 'String', 'ReadOnly' => 0, 'Desc' => 'Имя в MySQL', 'inList' => 1, 	'PlaceHolder' => '', 'PostText' => 'На английском, до 20 символов'),
				'Type' => array("Type" => 'List', 'ReadOnly' => 0, 'Desc' => 'Тип',	'inList' => 1,	'PlaceHolder' => '', 'Items' => array('Число', "Строка", "Многострочная строка", "Дата", "ЧекБокс", "Перечисление", "Перечисление объектов", "HTML", "Картинка", "Карта", "Файл")),
				'InList' => array('Type' => 'List', 'ReadOnly' => 0, 'Desc' => 'Видимость в списке', 'inList' =>1,	'PlaceHolder' => '', 'Items' => array('Не видно в списке', 'Видно в списке')),
				'ReadOnly' => array('Type' => 'List', 'ReadOnly' => 0, 'Desc' => 'Возможность редактировать', 'inList' => 0,	'PlaceHolder' => '', 'Items' => array('Можно редактировать', 'Нельзя редактировать')),
				'Obj_Tp' => array('Type' => 'ObjectList', 'ReadOnly' => 0, 'Desc' => 'Перечисление объектов: Объект перечисления', 'PlaceHolder' => '', 'inList' => 0,'Table' => array('Name' => 'editable', 'MainProper' => 'Name')),
				'Obj_Pl' => array('Type' => 'String',     'ReadOnly' => 0, 'Desc' => 'Перечисление объектов: Поле перечисления', 'PostText' => 'MySQL имя', 'PlaceHolder' => '', 'inList' => 0),
				'Img_maxwidth' => array('Type' => 'Integer',     'ReadOnly' => 0, 'Desc' => 'Картинка: максимальная ширина', 'PlaceHolder' => '', 'inList' => 0),
				'Img_maxheight' => array('Type' => 'Integer',     'ReadOnly' => 0, 'Desc' => 'Картинка: максимальная ширина', 'PlaceHolder' => '', 'inList' => 0),
				'Img_type' => array('Type' => 'List', 'ReadOnly' => 0, 'Desc' => 'Тип картинки', 'inList' => 0,	'PlaceHolder' => '', 'Items' => array('jpg', 'png', 'gif')),
				'Parent' => array('Type' => 'Integer', 'ReadOnly' => 1, 'Desc' => '# Родителя', 'inList' => 0)
			),
			'name' => 'Поля',
			'nameGen' => 'полей',
			'mysqlTabeName' => $name,
			'isObject' => 1,
			'isAddable' => true,
			'parent' => 'editable',
			'additionalPageButton' => array('url' => $GLOBALS['url_short'].'variants/%id%', 'name' => 'Варианты для перечисления'),
			'mainProper' => 'Name',
			'predName' => 'Поле "%name%"'
		);
		$GLOBALS[$name]['linkOne'] = $GLOBALS['url_short'].$GLOBALS[$name]['mysqlTabeName'].'/редактировать/%id%';
		$GLOBALS[$name]['linkOneR'] = $GLOBALS['url_short'].$GLOBALS[$name]['mysqlTabeName'].'/просмотр/%id%';
		$GLOBALS[$name]['linkCreate'] = $GLOBALS['url_short'].$GLOBALS[$name]['mysqlTabeName'].'/создать';
		$GLOBALS[$name]['linkFolder'] = $GLOBALS['url_short'].$GLOBALS[$name]['mysqlTabeName'];
		array_push($GLOBALS['items'], $GLOBALS[$name]);
	//------------------------------------------------------------------
		$name = 'variants';
		$GLOBALS[$name] = array(
			'items' => array(
				'ID' => array('Type' => 'Integer', 		'ReadOnly' => 1, 'Desc' => '#', 'inList' => 1),
				'Name' => array('Type' => 'String', 	'ReadOnly' => 0, 'Desc' => 'Название', 'inList' => 1,	'PlaceHolder' => ''),
				'Parent' => array('Type' => 'Integer', 'ReadOnly' => 1, 'Desc' => '# Родителя', 'inList' => 0)
			),
			'name' => 'Варианты',
			'nameGen' => 'вариантов',
			'mysqlTabeName' => $name,
			'isObject' => 1,
			'isAddable' => true,
			'parent' => 'rows',
		);
		$GLOBALS[$name]['linkOne'] = $GLOBALS['url_short'].$GLOBALS[$name]['mysqlTabeName'].'/редактировать/%id%';
		$GLOBALS[$name]['linkOneR'] = $GLOBALS['url_short'].$GLOBALS[$name]['mysqlTabeName'].'/просмотр/%id%';
		$GLOBALS[$name]['linkCreate'] = $GLOBALS['url_short'].$GLOBALS[$name]['mysqlTabeName'].'/создать';
		$GLOBALS[$name]['linkFolder'] = $GLOBALS['url_short'].$GLOBALS[$name]['mysqlTabeName'];
		array_push($GLOBALS['items'], $GLOBALS[$name]);
	//------------------------------------------------------------------
		$name = 'users';
		$GLOBALS[$name] = array(
			'items' => array(
				'ID' => array('Type' => 'Integer', 		'ReadOnly' => 1, 'Desc' => '#', 'inList' => 1),
				'email' => array('Type' => 'String', 	'ReadOnly' => 0, 'Desc' => 'Логин для входа', 'inList' => 1,	'PlaceHolder' => ''),
				'Name' => array('Type' => 'String', 	'ReadOnly' => 0, 'Desc' => 'Имя', 'inList' => 1,	'PlaceHolder' => ''),
				'Password' => array('Type' => 'String', 'ReadOnly' => 0, 'Desc' => 'Пароль', 'inList' => 0,	'PlaceHolder' => '', 'PostText' => "Не используйте одинаковые пароли ко всем сайтам, этот пароль будет виден всем"),
				'Type' => array('Type' => 'List', 'ReadOnly' => 0, 'Desc' => 'Тип', 'inList' => 0,	'PlaceHolder' => '', 'Items' => array('Администратор', 'Пользователь')),
			),
			'name' => 'Администраторы',
			'nameGen' => 'администраторов',
			'mysqlTabeName' => $name,
			'isObject' => 1,
			'isAddable' => true
		);
		$GLOBALS[$name]['linkOne'] = $GLOBALS['url_short'].$GLOBALS[$name]['mysqlTabeName'].'/редактировать/%id%';
		$GLOBALS[$name]['linkOneR'] = $GLOBALS['url_short'].$GLOBALS[$name]['mysqlTabeName'].'/просмотр/%id%';
		$GLOBALS[$name]['linkCreate'] = $GLOBALS['url_short'].$GLOBALS[$name]['mysqlTabeName'].'/создать';
		$GLOBALS[$name]['linkFolder'] = $GLOBALS['url_short'].$GLOBALS[$name]['mysqlTabeName'];
		array_push($GLOBALS['items'], $GLOBALS[$name]);
	//------------------------------------------------------------------
		$name = 'tokens';
		$GLOBALS[$name] = array(
			'items' => array(
				'ID' => array('Type' => 'Integer', 		'ReadOnly' => 1, 'Desc' => '#', 'inList' => 1),
				'token' => array('Type' => 'String', 	'ReadOnly' => 1, 'Desc' => 'Ключ', 'inList' => 0,	'PlaceHolder' => ''),
				'UID' => array('Type' => 'Integer', 	'ReadOnly' => 1, 'Desc' => 'Пользователь', 'inList' => 1)
			),
			'name' => 'Сессии',
			'nameGen' => 'сессий',
			'mysqlTabeName' => $name,
			'isObject' => 1,
			'isAddable' => false
		);
		$GLOBALS[$name]['linkOne'] = $GLOBALS['url_short'].$GLOBALS[$name]['mysqlTabeName'].'/редактировать/%id%';
		$GLOBALS[$name]['linkOneR'] = $GLOBALS['url_short'].$GLOBALS[$name]['mysqlTabeName'].'/просмотр/%id%';
		$GLOBALS[$name]['linkCreate'] = $GLOBALS['url_short'].$GLOBALS[$name]['mysqlTabeName'].'/создать';
		$GLOBALS[$name]['linkFolder'] = $GLOBALS['url_short'].$GLOBALS[$name]['mysqlTabeName'];
		array_push($GLOBALS['items'], $GLOBALS[$name]);
	//------------------------------------------------------------------
		//echo(json_encode($GLOBALS['items'], JSON_PRETTY_PRINT));
	//------------------------------------------------------------------

		foreach ($GLOBALS['items'] as $item) {

			$name = $item['mysqlTabeName'];
			$table = get_query("SHOW TABLES LIKE '" . $name . "'");
			if (is_null($table))
				write_query("CREATE TABLE `".$name."` ( `ID` BIGINT NOT NULL AUTO_INCREMENT , PRIMARY KEY (`ID`)) ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_general_ci");

			$fields = $item['items']; $has = arr_query("DESCRIBE `" . $name . "`"); $fieldsHas = array();
			foreach ($has as $fld) $fieldsHas[$fld['Field']] = $fld;
			foreach ($fields as $fieldName => $field) {
				if (isset($fieldsHas[$fieldName])) {
					$isOkay = true;
					switch ($field['Type']) {
						case 'Integer': $isOkay = (strpos($fieldsHas[$fieldName]['Type'], "int") !== false); break;
						case 'String': $isOkay = (strpos($fieldsHas[$fieldName]['Type'], "text") !== false); break;
						case 'ObjectList': $isOkay = (strpos($fieldsHas[$fieldName]['Type'], "int") !== false); break;
						case 'MultiLine': $isOkay = (strpos($fieldsHas[$fieldName]['Type'], "text") !== false); break;
						case 'Date': $isOkay = (strpos($fieldsHas[$fieldName]['Type'], "date") !== false); break;
						case 'CheckBox': $isOkay = (strpos($fieldsHas[$fieldName]['Type'], "int") !== false); break;
						case 'Special1': $isOkay = (strpos($fieldsHas[$fieldName]['Type'], "text") !== false); break;
						case 'Geo': $isOkay = (strpos($fieldsHas[$fieldName]['Type'], "text") !== false); break;
						case 'HTML': $isOkay = (strpos($fieldsHas[$fieldName]['Type'], "text") !== false); break;
						case 'List': $isOkay = (strpos($fieldsHas[$fieldName]['Type'], "text") !== false); break;
						case 'Image': $isOkay = (strpos($fieldsHas[$fieldName]['Type'], "text") !== false); break;
						case 'File': $isOkay = (strpos($fieldsHas[$fieldName]['Type'], "text") !== false); break;
						default: $isOkay= true;
					}
					if ($isOkay){
						continue;
					} else {
						write_query("ALTER TABLE `" . $name . "` DROP `".$fieldName."`");
					}
				}
				$type = '';
				switch ($field['Type']) {
					case 'Integer': $type='BIGINT'; break;
					case 'String': $type='LONGTEXT'; break;
					case 'ObjectList': $type='BIGINT'; break;
					case 'MultiLine': $type='LONGTEXT'; break;
					case 'Date': $type='DATE'; break;
					case 'CheckBox': $type='TINYINT(1)'; break;
					case 'Special1': $type='LONGTEXT'; break;
					case 'Geo': $type='LONGTEXT'; break;
					case 'HTML': $type='LONGTEXT'; break;
					case 'List': $type='LONGTEXT'; break;
					case 'Image': $type='LONGTEXT'; break;
					case 'File': $type='LONGTEXT'; break;
				}
				if ($type == '') continue;
				write_query("ALTER TABLE `".$name."` ADD `".$fieldName."` " . $type . " NOT NULL");
			}
			if (!in_array('LastUpdate', $fieldsHas))
				write_query("ALTER TABLE `".$name."` ADD `LastUpdate` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP");
			if (!in_array('Priority', $fieldsHas))
				write_query("ALTER TABLE `".$name."` ADD `Priority` BIGINT NOT NULL");

		}
	//------------------------------------------------------------------
		if (count(arr("users")) == 0)
			insert("users", array("email" => "admin", "Name" => "Администратор", "Password" => "start01", "Type" => 0 ))
	//------------------------------------------------------------------
?>
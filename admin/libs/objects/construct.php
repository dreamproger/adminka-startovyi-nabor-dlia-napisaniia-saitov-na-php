<?php 
//--------------------------------------------------------------------
	function listPage($object){
		changeDB();
		$keyObject = $object;
		$object = $GLOBALS[$object];

		ob_start ();
		include('pages/list.php');
		$c = ob_get_contents();
		ob_end_clean ();
		return $c;  
	}
	function editPage($object, $id){
		changeDB();
		$keyObject = $object;
		$object = $GLOBALS[$object];
		$item = get($object['mysqlTabeName'], $id);
		if (is_null($item)) exit('Данный объект не найден');

		ob_start ();
		include('pages/edit.php');
		$c = ob_get_contents();
		ob_end_clean ();
		return $c;  
	}
	function createPage($object){
		changeDB();
		$keyObject = $object;
		$object = $GLOBALS[$object];
		
		ob_start ();
		include('pages/create.php');
		$c = ob_get_contents();
		ob_end_clean ();
		return $c;  
	}
	function viewPage($object, $id){
		changeDB();
		$keyObject = $object;
		$object = $GLOBALS[$object];
		$item = get($object['mysqlTabeName'], $id);
		if (is_null($item)) exit('Данный объект не найден');

		ob_start ();
		include('pages/view.php');
		$c = ob_get_contents();
		ob_end_clean ();
		return $c;  
	}
	function sortPage($object){
		changeDB();
		$keyObject = $object;
		$object = $GLOBALS[$object];

		ob_start ();
		include('pages/sort.php');
		$c = ob_get_contents();
		ob_end_clean ();
		return $c;  
	}
//--------------------------------------------------------------------
	function getVKInfo($id){
		$id = explode('?', $id)[0];
		if (substr($id, 0, strlen("https://vk.com/id")) === "https://vk.com/id") $id = substr($id, strlen("https://vk.com/id")); else 
		if (substr($id, 0, strlen("https://m.vk.com/id")) === "https://m.vk.com/id") $id = substr($id, strlen("https://m.vk.com/id")); else 
		if (substr($id, 0, strlen("http://vk.com/id")) === "http://vk.com/id") $id = substr($id, strlen("http://vk.com/id")); else 
		if (substr($id, 0, strlen("http://m.vk.com/id")) === "http://m.vk.com/id") $id = substr($id, strlen("http://m.vk.com/id")); else 
		if (substr($id, 0, strlen("vk.com/id")) === "vk.com/id") $id = substr($id, strlen("vk.com/id")); else 
		if (substr($id, 0, strlen("http://vk.com/")) === "http://vk.com/") $id = substr($id, strlen("http://vk.com/")); else 
		if (substr($id, 0, strlen("https://vk.com/")) === "https://vk.com/") $id = substr($id, strlen("https://vk.com/")); else 
		if (substr($id, 0, strlen("https://m.vk.com/")) === "https://m.vk.com/") $id = substr($id, strlen("https://m.vk.com/")); else 
		if (substr($id, 0, strlen("m.vk.com/")) === "m.vk.com/") $id = substr($id, strlen("vk.com/")); else 
		if (substr($id, 0, strlen("vk.com/")) === "vk.com/") $id = substr($id, strlen("vk.com/"));
		$file = file_get_contents("https://api.vk.com/method/users.get?user_ids=".urlencode($id)."&fields=photo_200,photo_100,photo_50&access_token=".$GLOBALS['VK_serviceKey']);
		$answer = json_decode($file, true);
		return $answer['response'][0] ?? null;
	}
//--------------------------------------------------------------------
	function getValue($item, $object, $key){
		$polye = $object['items'][$key];
		$type = $polye['Type'];
		$value =  $item==null ? '' : $item[$key] ?? '';
		//-----------------------------------------------

		switch ($type) {
			case 'Integer': return $value;
			case 'String': return $value;
			case 'HTML': return $value;
			case 'MultiLine': return nl2br($value);
			case 'Date': return $value == '' ? date('d.m.Y') : date('d.m.Y', strtotime($value));
			case 'Image': return '<a target="_blank" href="'.$GLOBALS['constructHTTP'].str_replace('%id%', $item == null ? $temp : $item['ID'], $polye['path']).'?cache='.strtotime($item['LastUpdate']).'"><img style="margin-top: 20px; width: auto; height: auto; max-width: 300px; max-height: 300px;" src="'.$GLOBALS['constructHTTP'].str_replace('%id%',$item == null ? $temp : $item['ID'], $polye['path']).'?cache='.strtotime($item['LastUpdate']).'" data-id="'.($item == null ? $temp : $item['ID']).'"></a>' ;
			case 'File':
				if ($value != '') 
					return '<a target="_blank" href="'.$value.'">Просмотр файла</a>' ;
				else 
					return "Файл не загружен";
			case 'List': return $polye['Items'][$value] ?? ''; break;
			case 'CheckBox': if ($value == 1) return 'Включено'; else return 'Выключено'; break;
			case 'Special1': 
				if ($item['Type'] != 'Image')
					return (mb_strlen(strip_tags($value)) > 50 ? (mb_substr(strip_tags($value), 0, 47) . '...') : strip_tags($value));
				else 
					return  '<a target="_blank" href="'.$GLOBALS['constructHTTP'].'../'.str_replace('%id%', $item == null ? $temp : $item['ID'],trim(explode(',', $value)[0])).'?cache='.strtotime($item['LastUpdate']).'"><img style="margin-top: 20px; width: auto; height: auto; max-width: 300px; max-height: 300px;" src="'.$GLOBALS['constructHTTP'].'/'.str_replace('%id%',$item == null ? $temp : $item['ID'], trim(explode(',', $value)[0])).'?cache='.strtotime($item['LastUpdate']).'" data-id="'.($item == null ? $temp : $item['ID']).'"></a>';
			case 'Geo':
				if ($value == "") return "Координаты точки не заданы";
				$map = json_decode($value, true);
				$apt = $map['aptitude'];
				$lon = $map['longtitude'];
				$zoom = $map['zoom'];
				if ($map == "null" || $lon == "null" || $zoom == "null") return "Координаты точки не заданы";
				return "<div class='geoMapShow' data-apt='".$apt."' data-lon='".$lon."' data-zoom='".$zoom."'></div>";
			case 'ObjectList': 
				if ($value == 0) return ' - ';
				$obj = $GLOBALS[$polye['Table']['Name']];
				$itm = get($obj['mysqlTabeName'], $value);
				return '#' . $itm['ID'] . ' - ' . $itm[$polye['Table']['MainProper']];
			case 'VK':
				$user = getVKInfo($value);
				return "<div class='vk'><div style='background-image: url(".($user['photo_200']??$user['photo_100']??$user['photo_50']??'').")'></div><div><a href='https://vk.com/id".$user['uid']."' target='_blank'>".$user['first_name'].' '.$user['last_name']."</a></div></div>";
				return;
			break;
		//-----------------------------------------------
			default: return 'N/A';
		}
		//-----------------------------------------------
	}

//--------------------------------------------------------------------
	function brToNl($text){$breaks = array("<br />","<br>","<br/>");  return str_ireplace($breaks, "\r\n",str_replace("\n", "", $text));  }
	function br2nl2( $input ) {return preg_replace('/<br(\s+)?\/?>/i', "\n", $input);}
	function getEditValue($item, $object, $key){
		$polye = $object['items'][$key];
		$type = $polye['Type'];
		$value =  !is_array($item) ? '' : $item[$key] ?? '';
		$temp = !is_array($item) ? $item : 0;
		if (!is_array($item)) $item = null;
		//-----------------------------------------------
		switch ($type) {
			case 'Integer': return '<input type="number" id="f'.$item['ID'].'" '.($polye['ReadOnly']==1?'disabled':'').' value="'.$value.'" class="form-control col-md-7 col-xs-12 dataSave" data-row="'.$key.'">';
		//-----------------------------------------------
			case 'String': return '<input placeholder="'.htmlentities($polye['PlaceHolder'] ?? '').'" type="text" id="f'.$item['ID'].'" '.($polye['ReadOnly']==1?'disabled':'').' value="'.htmlentities($value).'" class="form-control col-md-7 col-xs-12 dataSave" data-row="'.$key.'">';
		//-----------------------------------------------
			case 'HTML': return '<div class="htmlEditor" data-row="'.$key.'">'.$value.'</div>';
			case 'MultiLine': return '<textarea placeholder="'.htmlentities($polye['PlaceHolder'] ?? '').'" type="text" id="f'.$item['ID'].'" '.($polye['ReadOnly']==1?'disabled':'').' class="form-control col-md-7 col-xs-12 dataSave" data-row="'.$key.'">'.brToNl($value).'</textarea>';
			case 'Date': return '<input type="text" class="form-control col-md-7 col-xs-12 dataSave date" data-row="'.$key.'" value="'.($value == '' ? date('d.m.Y') : date('d.m.Y', strtotime($value))).'">';
			case 'Image': 
				return '<a target="_blank" href="'.$GLOBALS['constructHTTP'].str_replace('%id%', $item == null ? $temp : $item['ID'], $polye['path']).'?cache='.strtotime($item['LastUpdate']).'"><img style="margin-top: 20px; width: auto; height: auto; max-width: 300px; max-height: 300px;" src="'.$GLOBALS['constructHTTP'].str_replace('%id%',$item == null ? $temp : $item['ID'], $polye['path']).'?cache='.strtotime($item['LastUpdate']).'" data-id="'.($item == null ? $temp : $item['ID']).'"></a>' . '<br> <br> <input class="image" accept="image/*" type="file" data-row="'.$key.'">' ;

			case 'File': 
				return '<a target="_blank" style="'.($value==''?'display: none':'').'" href="'.$value.'">Просмотр файла</a>' . '<br> <br> <input  type="file" class="file" data-row="'.$key.'">' ;
		//-----------------------------------------------
			case 'CheckBox':
				return ' <input type="checkbox" '.($value == 1?'checked':'').' data-row="'.$key.'" class="dataSaveCheckBox"> ';
		//-----------------------------------------------
			case 'List':
				$options = '';
				foreach ($polye['Items'] as $key2 => $value2) $options .= '<option '.($key2==$value?'selected':'').' value="'.$key2.'">' . $value2 . '</option>';
				return '<select id="f'.$item['ID'].'" '.($polye['ReadOnly']==1?'disabled':'').' class="form-control col-md-7 col-xs-12 dataSave" data-row="'.$key.'">'.$options.'</select>';
			case 'ObjectList':
				$obj = $GLOBALS[$polye['Table']['Name']];
				$itms = arr($obj['mysqlTabeName']);

				$options = '<option value="0"> - </option>';
				foreach ($itms as $itm) $options .= '<option '.($itm['ID']==$value?'selected':'').' value="'.$itm['ID'].'">' . '#' . $itm['ID'] . ' - ' . $itm[$polye['Table']['MainProper']] . '</option>';
				return '<select id="f'.$item['ID'].'" '.($polye['ReadOnly']==1?'disabled':'').' class="form-control col-md-7 col-xs-12 dataSave" data-row="'.$key.'">'.$options.'</select>';
		//-----------------------------------------------
			case 'Geo': 
				if ($value != "") $map = json_decode($value, true); else $map = array();
				$apt = $map['aptitude'] ?? 'null';
				$lon = $map['longtitude'] ?? 'null';
				$zoom = $map['zoom'] ?? 'null';
				return "<div class='geoMapEdit' data-row='".$key."' data-apt='".$apt."' data-lon='".$lon."' data-zoom='".$zoom."'></div>";
		//-----------------------------------------------
			case 'Special1': switch ($item['Type']) {
				case 'Text':
					return '<input placeholder="'.htmlentities($polye['PlaceHolder'] ?? '').'" type="text" id="f'.$item['ID'].'" '.($polye['ReadOnly']==1?'disabled':'').' value="'.htmlentities($value).'" class="form-control col-md-7 col-xs-12 dataSave" data-row="'.$key.'">';
				case 'MultiLine':
					return '<textarea placeholder="'.htmlentities($polye['PlaceHolder'] ?? '').'" type="text" id="f'.$item['ID'].'" '.($polye['ReadOnly']==1?'disabled':'').' class="form-control col-md-7 col-xs-12 dataSave" data-row="'.$key.'">'.br2nl2($value).'</textarea>';
				case 'HTML':
					return '<div class="htmlEditor" data-row="'.$key.'">'.$value.'</div>';
				case 'Image': return '<a target="_blank" href="'.$GLOBALS['constructHTTP'].'../'.str_replace('%id%', $item == null ? $temp : $item['ID'],trim(explode(',', $value)[0])).'?cache='.strtotime($item['LastUpdate']).'"><img style="margin-top: 20px; width: auto; height: auto; max-width: 300px; max-height: 300px;" src="'.$GLOBALS['constructHTTP'].str_replace('%id%',$item == null ? $temp : $item['ID'], trim(explode(',', $value)[0])).'?cache='.strtotime($item['LastUpdate']).'" data-id="'.($item == null ? $temp : $item['ID']).'"></a>' . '<br> <br> <input accept="image/*" type="file" data-row="'.$key.'">' ;
				default:
					return '<input placeholder="'.htmlentities($polye['PlaceHolder'] ?? '').'" type="text" id="f'.$item['ID'].'" '.($polye['ReadOnly']==1?'disabled':'').' value="'.htmlentities($value).'" class="form-control col-md-7 col-xs-12 dataSave" data-row="'.$key.'">';
			}
		//-----------------------------------------------
			case 'VK': 
				$link = '';
				if ($value != '') if ($value != 0) $link='https://vk.com/id'.$value;
				return '<input placeholder="'.htmlentities($polye['PlaceHolder'] ?? '').'" type="text" id="f'.$item['ID'].'" '.($polye['ReadOnly']==1?'disabled':'').' value="'.htmlentities($link).'" class="form-control col-md-7 col-xs-12 dataSave" data-row="'.$key.'"> <br> Ссылка на страницу ВК, пример https://vk.com/dreamproger';
		//-----------------------------------------------
			default: return 'N/A';
		}
		//-----------------------------------------------
	}
//--------------------------------------------------------------------
	function getSaveValue($object, $key, $value){
		$polye = $object['items'][$key];
		$type = $polye['Type'];
		//-----------------------------------------------
		switch ($type) {
			case 'Integer': return is_numeric($value) ? $value : 0;
			case 'CheckBox': return is_numeric($value) ? $value : 0;
		//-----------------------------------------------
			case 'String': return $value;
			case 'Geo': return $value;
		//-----------------------------------------------
			case 'HTML': return $value;
			case 'MultiLine': return nl2br($value);
			case 'Date': return date('Y-m-d', strtotime($value));
			case 'Image': return null;
			case 'File': return null;
			case 'Special1': return $value;
		//-----------------------------------------------
			case 'List': return $value;
			case 'ObjectList': return $value;
		//-----------------------------------------------
			case 'VK':
				$user = getVKInfo($value);
				if (!isset($user['uid'])) return 0;

				return $user['uid'];

		//-----------------------------------------------
			default: return null;
		}
		//-----------------------------------------------
	}
//--------------------------------------------------------------------
	function genNav($object, $item, $parentID, $additionalText){
		$text = '';
		if (isset($object['parent']) && $parentID == null && $item != null) $parentID = $item['Parent'];
		if (isset($object['parent']) && $parentID != null) {
			$parent = $GLOBALS[$object['parent']];
			$itemParent = get($parent['mysqlTabeName'], $parentID);
			$text .= genNav($parent, $itemParent, $itemParent['Parent']??null, null);
		}
		$text .= " / <a href='".$object['linkFolder'].($parentID!=null?'/'.$parentID:'')."'>".$object['name']."</a>";
		if ($item != null) $text .= " / " . "<a href='".str_replace("%id%", $item['ID'], $object['linkOne'])."'>".(isset($object['mainProper'])?str_replace("%name%",$item[$object['mainProper']],$object['predName']):'Объект №'.$item['ID'])."</a>";
		if ($additionalText != null) $text .= " / " . $additionalText;
		return $text;
	}
//--------------------------------------------------------------------
	function b($item, $object, $key){
		$object = $GLOBALS[$object];
		//------------------------------------------------------------
		$polye = $object['items'][$key];
		$type = $polye['Type'];
		$value =  $item==null ? '' : $item[$key] ?? '';
		//------------------------------------------------------------
		switch ($type) {
			case 'Integer': return $value;
			case 'String': return $value;
			case 'HTML': return $value;
			case 'MultiLine': return $value;
			case 'Date': return $value == '' ? date('d.m.Y') : date('d.m.Y', strtotime($value));
			case 'Image': return '/'.str_replace('%id%',$item == null ? $temp : $item['ID'], $polye['path']).'?cache='.strtotime($item['LastUpdate']);
			case 'File': return '/'.str_replace('%id%',$item == null ? $temp : $item['ID'], $polye['path']).'?cache='.strtotime($item['LastUpdate']);
			case 'List': return $polye['Items'][$value] ?? ''; break;
			case 'CheckBox': return $value;
			case 'Special1': 
				if ($item['Type'] != 'Image')
					return strip_tags($value);
				else 
					return  '<img style="margin-top: 20px; width: auto; height: auto; max-width: 300px; max-height: 300px;" src="'.$GLOBALS['constructHTTP'].'/'.str_replace('%id%',$item == null ? $temp : $item['ID'], trim(explode(',', $value)[0])).'?cache='.strtotime($item['LastUpdate']).'" data-id="'.($item == null ? $temp : $item['ID']).'">';
			case 'Geo':
				$map = json_decode($value, true);
				$apt = $map['aptitude'];
				$lon = $map['longtitude'];
				$zoom = $map['zoom'];
				return array($apt, $lon, $zoom);
			case 'ObjectList': 
				return $itm['ID'];
			break;
		//------------------------------------------------------------
			default: return '';
		}
		//------------------------------------------------------------
	}
	function f($object, $id){
		$a = get($object, 'ID', $id);
		$value = $a['Value'];
		//------------------------------------------------------------
		switch ($a['Type']) {
			case 'Text': return $value;
			case 'MultiLine': return $value;
			case 'HTML': return $value;
			case 'Image': return '/'.str_replace('%id%',$item == null ? $temp : $item['ID'], explode(',',$value)[0]).'?cache='.strtotime($a['LastUpdate']);
			case 'File': return '/'.str_replace('%id%',$item == null ? $temp : $item['ID'], explode(',',$value)[0]).'?cache='.strtotime($a['LastUpdate']);
		//------------------------------------------------------------
			default: return '';
		}
		//------------------------------------------------------------
	}
//--------------------------------------------------------------------
?>
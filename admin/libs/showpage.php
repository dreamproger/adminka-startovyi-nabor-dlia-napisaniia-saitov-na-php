<?php
/* -------------------------------------------------------------------------- */
	$page = urldecode($_SERVER['REQUEST_URI'] );
	$page = substr($page, 0);
	$pages = explode('/', $page);
	$page = $pages[2];
	$GLOBALS['nowpage'] = $page;
/* -------------------------------------------------------------------------- */
	if (!$GLOBALS['logged']) {
		if (mb_strtolower($page) != 'вход')  gopage('вход');
	}
/* -------------------------------------------------------------------------- */
	
/* -------------------------------------------------------------------------- */
	switch (mb_strtolower($page)) {
		case 'вход':
			if ($GLOBALS['logged']) { gopage('главная'); } else { $page = page('login', $pages);}
			break;
		case 'главная':
			$page = page('index', $pages);
			break;
		case 'статистика':
			$page = page('statisticByDay', $pages);
			break;
		case '':
			if (!$GLOBALS['logged']) gopage('Вход'); else gopage('главная');
			break;
		case 'выход':
			dislog();
			gopage('вход');
			break;
		default:
			foreach ($GLOBALS['items'] as $table2) {
				$table = $table2['myname'] ?? $table2['mysqlTabeName'];
				if (mb_strtolower($table) == mb_strtolower($page)){
					switch (mb_strtolower($pages[3]??'')) {
						case 'создать':
							if (isset($table2['parent']) || ($table2['parent']??0) != 0)
								if (isset($pages[4])) $GLOBALS['parentElement'] = $pages[4];
							$page = createPage($table);
							break;
						case 'просмотр':
							if (is_numeric($pages[4])) $page = viewPage($table, $pages[4]); else gopage('проекты');
							break;
						case 'редактировать':
							if (is_numeric($pages[4])) $page = editPage($table, $pages[4]); else gopage('проекты');
							break;
						case 'сортировка':
							if (isset($table2['parent']) || ($table2['parent']??0) != 0)
								if (isset($pages[3])) $GLOBALS['parentElement'] = $pages[3];
							$page = sortPage($table);
							break;
						case '':
							if (isset($table2['parent']) || ($table2['parent']??0) != 0)
								if (isset($pages[3])) $GLOBALS['parentElement'] = $pages[3];
							$page = listPage($table);
							break;
						default:
							if (isset($table2['parent']) || ($table2['parent']??0) != 0)
								if (isset($pages[3])) $GLOBALS['parentElement'] = $pages[3];
							$page = listPage($table);
							break;
					}
					break;
				}
			}
			break;
	}
/* -------------------------------------------------------------------------- */
	$shablon = get_template($GLOBALS['shablon']);
	$menu = get_menus();
	$html = str_replace('%menu%', $menu, $shablon);
	$html = str_replace('%css%', $GLOBALS['css'], $html );
	$html = str_replace('%title%', $GLOBALS['title'], $html );
	$html = str_replace('%header%', $GLOBALS['header'], $html );
	$html = str_replace('%uName%', $GLOBALS['user']['Name'], $html );
	$html = str_replace('%content%', $page, $html );
	$html = str_replace('%base%',  $GLOBALS['url_short'] , $html );
/* -------------------------------------------------------------------------- */
	echo($html);
/* -------------------------------------------------------------------------- */
?>
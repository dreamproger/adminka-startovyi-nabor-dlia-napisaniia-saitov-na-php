<?php
changeDB();
$GLOBALS['pageLatest'] = -1;
//------------------------------------------------------------
$user = $GLOBALS['user'];
if ($user == null) die();
//------------------------------------------------------------
function deleteDir($dirPath) {
    if (! is_dir($dirPath)) {
        throw new InvalidArgumentException("$dirPath must be a directory");
    }
    if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
        $dirPath .= '/';
    }
    $files = glob($dirPath . '*', GLOB_MARK);
    foreach ($files as $file) {
        if (is_dir($file)) {
            deleteDir($file);
        } else {
            unlink($file);
        }
    }
    rmdir($dirPath);
}
//------------------------------------------------------------
switch ($query) {
	case 'saveImage':
		if (($GLOBALS[$args['object']] ?? 0) == 1) die();
		$object = $GLOBALS[$args['object']];
		$polye = $object['items'][$args['row']];
		if ($polye['Type'] == 'Special1'){
			$item = get($object['mysqlTabeName'], $args['id']);
			$zn = explode(',', $item['Value']);
			$polye['path'] = trim($zn[0]);
			$polye['maxwidth'] = trim($zn[1]??3000);
			$polye['maxheight'] = trim($zn[2]??3000);
		}
		//----------------------------------------------------

		try {
			if (($polye['createFolder'] ?? '') != '') mkdir($GLOBALS['constructFolder'].str_replace('%id%', $args['id'], $polye['createFolder']), 0777, true);
		} catch (Exception $e) {
			
		}
		//----------------------------------------------------
		update($object['mysqlTabeName'], array('ID' => $args['id'],'LastUpdate' => date('Y-m-d H:i:s'), $args['row'] => $GLOBALS['constructHTTP'].str_replace('%id%',$item == null ? $temp : $args['id'], $polye['path']).'?cache='.time() ));
		//----------------------------------------------------
		@include('libs/imageresize.php');
		$image = new SimpleImage($_FILES['file']['tmp_name']);
		$SavePath = $GLOBALS['constructFolder'].str_replace('%id%', $args['id'], $polye['path']);
		$image->best_fit($polye['maxwidth'], $polye['maxheight']);
		$image->save($SavePath);
		try {
			touch($SavePath, time());
		} catch (Exception $e) {
			
		}
		if (($polye['prev'] ?? '') != ''){
			$image->thumbnail($polye['prevW'], $polye['prevH']);
			$image->save($GLOBALS['constructFolder'].str_replace('%id%', $args['id'], $polye['prev']));
		}
		$response = 1;
		//----------------------------------------------------
		break;
	case 'saveFile':
		if (($GLOBALS[$args['object']] ?? 0) == 1) die();
		$object = $GLOBALS[$args['object']];
		$polye = $object['items'][$args['row']];
		//----------------------------------------------------
		// Чистим папку
		try {
			if (($polye['createFolder'] ?? '') != '') @deleteDir($GLOBALS['constructFolder'].str_replace(array('%id%', '%file%'), array($args['id'], ''), $polye['createFolder']));
		} catch (Exception $e) {
			
		}
		// Создаем ее заново
		try {
			if (($polye['createFolder'] ?? '') != '') @mkdir($GLOBALS['constructFolder'].str_replace(array('%id%', '%file%'), array($args['id'], ''), $polye['createFolder']), 0777, true);
		} catch (Exception $e) {
			
		}
		//----------------------------------------------------
		$file = str_replace('/', '_', $_FILES['file']['name']);
		$SavePath = $GLOBALS['constructFolder'].str_replace(array('%id%', '%file%'), array($args['id'], $file), $polye['path']);
		move_uploaded_file($_FILES['file']['tmp_name'], $SavePath);
		//----------------------------------------------------
		update($object['mysqlTabeName'], 
			array(
				'ID' => $args['id'],'LastUpdate' => date('Y-m-d H:i:s'),
				$args['row'] => $GLOBALS['constructHTTP'].str_replace(array('%id%', '%file%'),array($item == null ? $temp : $args['id'], $file), $polye['path']).'?cache='.time()
			));
		 
		//----------------------------------------------------
		$otv['file'] = $GLOBALS['constructHTTP'].str_replace(array('%id%', '%file%'),array($item == null ? $temp : $args['id'], $file), $polye['path']).'?cache='.time();
		$response = 1;
		//----------------------------------------------------
		break;
	case 'save':
		if (($GLOBALS[$args['object']] ?? 0) == 1) die();
		$object = $GLOBALS[$args['object']];
		//----------------------------------------------------
		$newO = array('ID' => $args['id']);
		foreach ($object['items'] as $key => $value) {
			$v = getSaveValue($object, $key, $args[$key]);
			if ($v == null) continue;
			$newO[$key] = $v;
		}
 
		update($object['mysqlTabeName'], $newO);
		$response = 1;
		break;
	case 'sort':
		if (($GLOBALS[$args['object']] ?? 0) == 1) die();
		$object = $GLOBALS[$args['object']];
		//----------------------------------------------------
		$sort = json_decode($args['ids'], true);
		foreach ($sort as $i => $id) {
			$position = $i + 1;
			write_query("UPDATE `".$object['mysqlTabeName']."` SET `Priority` = %i where `ID` = %i", $position, $id);
		}
		//----------------------------------------------------
		$response = 1;
		break;
	case 'remove':
		if (($GLOBALS[$args['object']] ?? 0) == 1) die();
		$object = $GLOBALS[$args['object']];
		//----------------------------------------------------
		remove($object['mysqlTabeName'], $args['id']);
		foreach ($object['items'] as $key => $value) {
			if ($value['Type'] != 'Image') continue;
			unlink($GLOBALS['constructFolder'].str_replace('%id%', $args['id'], $value['path']));
		}
		$response = 1;
		break;
}
//------------------------------------------------------------
	if (($GLOBALS[$args['object']] ?? 0) == 1) die();
	$object = $GLOBALS[$args['object']];
	if ($args['object'] == 'Editors') file_put_contents("/var/www/my/mylittlechat/java/tempWithPHPCabinet/forceReload", "1");
//------------------------------------------------------------
restoreDB();
//------------------------------------------------------------
?>
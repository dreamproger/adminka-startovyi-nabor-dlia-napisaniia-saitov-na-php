<?php
changeDB();
$GLOBALS['pageLatest'] = -1;
//------------------------------------------------------------
$user = $GLOBALS['user'];
if ($user == null) die();
//------------------------------------------------------------
switch ($query) {
	case 'saveImage':
		if (($GLOBALS[$args['object']] ?? 0) == 1) die();
		$object = $GLOBALS[$args['object']];
		$polye = $object['items'][$args['row']];
		if (is_null($polye)) die();
		//----------------------------------------------------
		include('libs/imageresize.php');
		$image = new SimpleImage($_FILES['file']['tmp_name']);
		$SavePath = $GLOBALS['constructFolder'].str_replace('%id%', $args['temp'], $polye['path']);
		$image->best_fit($polye['maxwidth'], $polye['maxheight']);
		$image->save($SavePath);
		$response = 1;
		//----------------------------------------------------
		break;
	case 'create':
		if (($GLOBALS[$args['object']] ?? 0) == 1) die();
		$object = $GLOBALS[$args['object']];
		//----------------------------------------------------
		$newO = array();
		foreach ($object['items'] as $key => $value) {
			$v = getSaveValue($object, $key, $args[$key]);

			if ($v == null) continue;
			$newO[$key] = $v;
		}
		//----------------------------------------------------
		if (isset($args['parent'])) $newO['Parent'] = $args['parent'];
		//----------------------------------------------------
		$id = insert($object['mysqlTabeName'], $newO);
		if (!is_numeric($id)) exit(json_encode(array('response' => 0, 'error' => 'Внутренняя ошибка - ' . $id)));
		//----------------------------------------------------
		if (($object['createFolder'] ?? '') != '') mkdir($GLOBALS['constructFolder'].str_replace('%id%', $id, $object['createFolder']));
		//----------------------------------------------------
		$response = 1;
		$otv['link'] = str_replace('%id%', $id, $object['linkOne']);
		break;
}
//------------------------------------------------------------
restoreDB();
//------------------------------------------------------------
?>
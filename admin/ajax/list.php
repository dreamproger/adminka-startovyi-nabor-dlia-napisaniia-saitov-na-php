<?php
changeDB();
$GLOBALS['pageLatest'] = -1;
//------------------------------------------------------------
$user = $GLOBALS['user'];
if ($user == null) die();
//------------------------------------------------------------
switch ($query) {
	case 'get':
		if (($GLOBALS[$args['object']] ?? 0) == 1) die();
		//----------------------------------------------------
		$object = $GLOBALS[$args['object']];
		$query = "SELECT * FROM `".$object['mysqlTabeName']."` WHERE 1"; $filter = '';

		//----------------------------------------------------
		if (isset($args['parent'])) $filter .= ' AND `Parent` = '.$args['parent'];
		//----------------------------------------------------
		// Фильтры
		$query .= $filter;
		//----------------------------------------------------
		// Сортировка
		$ord = $args['order'];
		if ($ord == "ID") $ord = 'Priority`, `ID';
		if (!is_array($object['items'][$ord]??'')) $ord = 'Priority`, `ID';
		$query .= ' ORDER BY `' . $ord . '` ';
		//----------------------------------------------------
		// Обратная ли сортировка
		$orders = array(
			'0' => '',
			'1' => 'DESC'
		); if (isset($orders[($args['desc'] ?? '0')])) $desc = $orders[($args['desc'] ?? '0')]; else $desc = '';
		$query .= $desc . ' ';
		//----------------------------------------------------
		// Страница
		$page = $args['page'] ?? 1; $byPage = $args['atPage'] ?? 10;
		$start = ($page - 1) * $byPage;
		$query .= ' LIMIT ' . $start . ',' . $byPage;
		//----------------------------------------------------
		
		$pays = arr_query($query);
		$otv['html'] = '';
		foreach ($pays as $pay) $otv['html'] .= getHTML($object, $pay);
		//----------------------------------------------------
		// Переключатель
		$maxCount = get_query('SELECT COUNT(*) FROM `'.$object['mysqlTabeName'].'` WHERE 1' . $filter)['COUNT(*)'];
		$maxPage = ceil($maxCount / $byPage);
		$otv['pages'] = getPagesHTML($maxPage, $page);
		$otv['count'] = $maxCount;
		break;
}
restoreDB();
//------------------------------------------------------------

function getHTML($object, $item){
	$html = '<tr style="cursor:pointer" onclick="window.location.href=\''.str_replace('%id%', $item['ID'], $object['linkOneR']).'\'">';
	foreach ($object['items'] as $key => $value)if ($value['inList'] == 1) $html .= '<td data-id="'.$item['ID'].'">' . getValue($item, $object, $key) . '</td>';
	$html .= '</tr>';
	return $html;
}
function getPagesHTML($max, $current){
	if ($max <= 6) {
		$html = '';
		for ($i=1; $i <= $max ; $i++) $html .= getPagesHTMLSingle($i, $i == $current);
		return $html;
	}
	$op = array(1,2,$max-1,$max);
	$html = '';
	$html .= getPagesHTMLSingle(1, 1 == $current);
	$html .= getPagesHTMLSingle(2, 2 == $current);
	if (!in_array($current - 1, $op)) $html .= getPagesHTMLSingle($current - 1, false);
	if (!in_array($current, $op)) $html .= getPagesHTMLSingle($current, true);
	if (!in_array($current + 1, $op)) $html .= getPagesHTMLSingle($current + 1, false);
	$html .= getPagesHTMLSingle($max - 1, ($max - 1 == $current));
	$html .= getPagesHTMLSingle($max, ($max == $current));
	return $html;
}
function getPagesHTMLSingle($i, $now){
	if ($i == 0) return; 
	$r = '';
	if ($GLOBALS['pageLatest'] < 0) $r = ''; else 
	if (abs($GLOBALS['pageLatest'] - $i) <= 1 ) $r = ' '; else $r = ' <li class="paginate_button"> <a> ... </a> </li> ';
	$GLOBALS['pageLatest'] = $i;

	return '<li class="paginate_button ' . ($now ? 'active' : '' ) . '" style="cursor: pointer" onclick="window.goPage('.$i.');"> <a href="#">' . $i . '</a></li>';
}
//------------------------------------------------------------
?>
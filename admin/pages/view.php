<?php 
changeDB();
//--------------------------------------------------------------------------------
	$GLOBALS['css'] = 'main';
	$GLOBALS['shablon'] = 'main';
	$GLOBALS['title'] = 'Просмотр объекта ' . $item['ID'] . ' - ' . $object['name'];
	$GLOBALS['header'] = 'Просмотр объекта ' . $item['ID'] . ' - '  . $object['name'];
//--------------------------------------------------------------------------------
if (isset($object['parent'])) $parent = $item['Parent'];
//--------------------------------------------------------------------------------
?>
<div style="margin-bottom: 10px;" class='nav'>
  <?php /* <a href='/'>Главная</a> <?php if(isset($parent)) echo(" / <a href='".$GLOBALS[$object['parent']]['linkFolder']."'>".$GLOBALS[$object['parent']]['name'].'</a> / ' . "<a href='".str_replace('%id%', $parent, $GLOBALS[$object['parent']]['linkOne'])."'> ".(isset($GLOBALS[$object['parent']]['mainProper']) ? str_replace("%name%", get($GLOBALS[$object['parent']]['mysqlTabeName'], 'ID', $parent)[$GLOBALS[$object['parent']]['mainProper']], $GLOBALS[$object['parent']]['predName']) : 'Объект №'. $parent)."</a>"); ?> / <?php echo("<a href='".(isset($parent)?$object['linkFolder'].'/'.$parent:$object['linkFolder'])."'>".$object['name']."</a>"); ?> / <?php echo(isset($object['mainProper']) ? str_replace("%name%", $item[$object['mainProper']], $object['predName']) : 'Объект №'. $item['ID']); ?> / Просмотр */ ?>
  <a href='%base%'>Главная</a><?php echo(genNav($object, $item, $parent??null,'Просмотр')); ?>
</div>
<div class="x_panel">
    <div class="x_title">
        <h2> <?php echo('Просмотр объекта ' . $item['ID']); ?> <small> <?php echo($object['name']); ?> </small></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <br>
        <form data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
        	<?php 
        		foreach ($object['items'] as $key => $value) {
        	?>
        	<div class="form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="f<?php echo($key); ?>"><?php echo($value['Desc']); ?></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div style='padding-top: 10px; display: inline-block; width: 100%;'><?php echo(getValue($item, $object, $key)); ?></div>
                    <span class='status'></span>
                </div>
            </div>

        	<?php 
        		}
        	?>
			
             
       
      	 </form>
        <div class="ln_solid"></div>
      	<div class="form-group">
        	<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
          		<a href='<?php echo(isset($parent)?$object['linkFolder'].'/'.$parent:$object['linkFolder']); ?>'><button type="submit" class="btn  btn-default">Назад</button></a>
          		<a href='<?php echo(str_replace('%id%', $item['ID'], $object['linkOne'])); ?>'><button type="submit" class="btn btn-primary">Редактировать</button></a>
              <?php if (($object['additionalPageButton'] ?? '' ) != ''){ ?>
              <a href='<?php echo(str_replace('%id%', $item['ID'], $object['additionalPageButton']['url'])); ?>'><span class="btn btn-info"><?php echo($object['additionalPageButton']['name']); ?></span></a>
              <?php } ?><?php if (($object['additionalPageButton1'] ?? '' ) != ''){ ?>
                <a href='<?php echo(str_replace('%id%', $item['ID'], $object['additionalPageButton1']['url'])); ?>'><span class="btn btn-info"><?php echo($object['additionalPageButton1']['name']); ?></span></a>
                <?php } ?><?php if (($object['additionalPageButton2'] ?? '' ) != ''){ ?>
                <a href='<?php echo(str_replace('%id%', $item['ID'], $object['additionalPageButton2']['url'])); ?>'><span class="btn btn-info"><?php echo($object['additionalPageButton2']['name']); ?></span></a>
                <?php } ?><?php if (($object['additionalPageButton3'] ?? '' ) != ''){ ?>
                <a href='<?php echo(str_replace('%id%', $item['ID'], $object['additionalPageButton3']['url'])); ?>'><span class="btn btn-info"><?php echo($object['additionalPageButton3']['name']); ?></span></a>
                <?php } ?><?php if (($object['additionalPageButton4'] ?? '' ) != ''){ ?>
                <a href='<?php echo(str_replace('%id%', $item['ID'], $object['additionalPageButton4']['url'])); ?>'><span class="btn btn-info"><?php echo($object['additionalPageButton4']['name']); ?></span></a>
                <?php } ?><?php if (($object['additionalPageButton5'] ?? '' ) != ''){ ?>
                <a href='<?php echo(str_replace('%id%', $item['ID'], $object['additionalPageButton5']['url'])); ?>'><span class="btn btn-info"><?php echo($object['additionalPageButton5']['name']); ?></span></a>
                <?php } ?><?php if (($object['additionalPageButton6'] ?? '' ) != ''){ ?>
                <a href='<?php echo(str_replace('%id%', $item['ID'], $object['additionalPageButton6']['url'])); ?>'><span class="btn btn-info"><?php echo($object['additionalPageButton6']['name']); ?></span></a>
                <?php } ?><?php if (($object['additionalPageButton7'] ?? '' ) != ''){ ?>
                <a href='<?php echo(str_replace('%id%', $item['ID'], $object['additionalPageButton7']['url'])); ?>'><span class="btn btn-info"><?php echo($object['additionalPageButton7']['name']); ?></span></a>
                <?php } ?>
          		<br><br>
          		<span class='stat'></span>
        	</div>
      	</div>
    </div>
</div>

<script type="text/javascript">$(document).ready(function(){onLoadObjects();});</script>
 <?php 
 restoreDB();
 ?>
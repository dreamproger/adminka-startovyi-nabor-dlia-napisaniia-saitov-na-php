<?php 
	changeDB();
//--------------------------------------------------------------------------------
	$GLOBALS['css'] = 'main';
	$GLOBALS['shablon'] = 'main';
	$GLOBALS['title'] = 'Редактирование объекта ' . $item['ID'] . ' - ' . $object['name'];
	$GLOBALS['header'] = 'Редактирование объекта ' . $item['ID'] . ' - '  . $object['name'];
//--------------------------------------------------------------------------------
	if (isset($object['parent'])) $parent = $item['Parent'];
//--------------------------------------------------------------------------------
?>
<div style="margin-bottom: 10px;" class='nav'>
	<?php /* <a href='/'>Главная</a><?php if(isset($parent)) echo(" / <a href='".$GLOBALS[$object['parent']]['linkFolder']."'>".$GLOBALS[$object['parent']]['name'].'</a> / ' . "<a href='".str_replace('%id%', $parent, $GLOBALS[$object['parent']]['linkOne'])."'> ".(isset($GLOBALS[$object['parent']]['mainProper']) ? str_replace("%name%", get($GLOBALS[$object['parent']]['mysqlTabeName'], 'ID', $parent)[$GLOBALS[$object['parent']]['mainProper']], $GLOBALS[$object['parent']]['predName']) : 'Объект №'. $parent)."</a>"); ?> / <?php echo("<a href='".(isset($parent)?$object['linkFolder'].'/'.$parent:$object['linkFolder'])."'>".$object['name']."</a>"); ?> / <?php echo(isset($object['mainProper']) ? str_replace("%name%", $item[$object['mainProper']], $object['predName']) : 'Объект №'. $item['ID']); ?> / Редактировать */ ?>
	<a href='%base%'>Главная</a><?php echo(genNav($object, $item, $parent??null,'Редактировать')); ?>
</div>
<div class="x_panel">
    <div class="x_title">
        <h2> <?php echo('Редактирование объекта ' . $item['ID']); ?> <small> <?php echo($object['name']); ?> </small></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <br>
        <form data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
        	<?php 
        		foreach ($object['items'] as $key => $value) {

        	?>
        	<div class="form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="f<?php echo($key); ?>"><?php echo($value['Desc']); ?></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php echo(getEditValue($item, $object, $key)); ?> <br>
                    <span class='status'><?php echo($value['PostText']??''); ?></span>
                </div>
            </div>

        	<?php 
        		}
        	?>
			
             
       
	        <div class="ln_solid"></div>
	      	<div class="form-group">
	        	<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	          		<a href='<?php echo(str_replace('%id%', $item['ID'], isset($parent)?$object['linkFolder'].'/'.$parent:$object['linkFolder'])); ?>'><span class="btn btn-default">Отмена</span></a>
	          		<button type="submit" class="btn btn-success">Сохранить</button>
	          		<?php if($object['isAddable']) { ?><button class="btn btn-danger remove">Удалить</button><?php } ?>
	          		 
	          		<br><br>
	          		<span class='stat'></span>
	        	</div>
	      	</div>
      	 </form>
    </div>
</div>
<script type="text/javascript" src='%base%vendors/ckeditor/ckeditor.js'></script>
<script type="text/javascript" src='%base%vendors/ckeditor/lang/ru.js'></script>
 
<link rel="stylesheet" href="%base%vendors/jquery-spellchecker-master/css/jquery.spellchecker.css" />
<script type="text/javascript" src='%base%vendors/jquery-spellchecker-master/js/jquery.spellchecker.js'></script>
<script>
$(document).ready(function() {
	moment.locale('ru');

	$('.date').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
    }, 
    function(start, end, label) {
        //var years = moment().diff(start, 'years');
        //alert("You are " + years + " years old.");
    });
});
var editors = {};
$(document).ready(function() {
	$('.htmlEditor').each(function(index, el) {
		$(this).parents('.form-group').after(this);

		editors[$(this).attr('data-row')] = CKEDITOR.replace(this, { language: 'ru'});	
		 
	});
});
//-------------------------------------------------------------------------------------------
$(document).ready(function() {
	$('input[type=file]').change(function(event) {
		var stat = $(this).parent().find('.status');
		var img = $(this).parent().find('img'); 
		var a = $(this).parent().find('a'); 
		DoFileQuery('edit', $(this).hasClass('image') ? 'saveImage' : 'saveFile', {row: $(this).attr('data-row'), object: "<?php echo($keyObject); ?>", id: <?php echo($item['ID']); ?>}, function(proc){
			stat.text("Идет загрузка " + proc + '%');
		}, function(err){
			stat.text("Ошибка " + err);
		}, function(otv){
			stat.text("Загрузка файла успешно завершена");
			if (otv.file != null){
				a.attr('href', otv.file).css('display', 'inline');
				return;
			}
		 
			var url = img.attr('src').split('?')[0];
			img.attr('src', url+"?"+ Math.round(new Date().getTime()/1000));

		}, this);
	});
});
//-------------------------------------------------------------------------------------------
var tim = null;
$("form").submit(function(e) {
	clearTimeout(tim);
	var args = {object: "<?php echo($keyObject); ?>", id: <?php echo($item['ID']); ?>};
	$('.dataSave').each(function(index, el) {
		args[$(this).attr('data-row')] = $(this).val();
	});
	$('.dataSaveCheckBox').each(function(index, el) {
		args[$(this).attr('data-row')] = $(this).prop('checked') ? 1 : 0;
	});
	$.each(editors, function(index, val) {
		args[index] = val.getData();
	});
	$('.geoMapEdit').each(function(index, el) {
		if (this.getCoordinates() == null) return;
		args[$(this).attr('data-row')] = this.getCoordinates();
	});
	$('.stat').text("Сохранение");
	DoQuery('edit', 'save', args, function(otv){
		clearTimeout(tim);
		if (otv.response == 1){
			$('.stat').text("Сохранение завершено");
			tim = setTimeout(function(){ $('.stat').text(""); }, 5000);
			window.location.href='<?php echo(str_replace('%id%', $item['ID'], $object['linkOneR'])); ?>';
		} else {
			$('.stat').text("Произошла ошибка, попробуйте позже");
		}
	});
    e.preventDefault();
});
//-------------------------------------------------------------------------------------------
$('.remove').click(function(e) {
	e.preventDefault();
	if (!confirm('Вы уверены, что вы хотите удалить этот объект?')) return;
	$('.stat').text("Удаление");
	var args = {object: "<?php echo($keyObject); ?>", id: <?php echo($item['ID']); ?>};
	DoQuery('edit', 'remove', args, function(otv){
		clearTimeout(tim);
		if (otv.response == 1){
			$('.stat').text("Удаление завершено");
			tim = setTimeout(function(){ $('.stat').text(""); }, 5000);
			window.location.href='<?php echo(isset($parent)?$object['linkFolder'].'/'.$parent:$object['linkFolder']); ?>';
		} else {
			$('.stat').text("Произошла ошибка, попробуйте позже");
		}
	});
});

$(document).ready(function(){onLoadObjects();});
</script>
<?php 
 restoreDB();
 ?>
<?php 
//--------------------------------------------------------------------------------
changeDB();
//--------------------------------------------------------------------------------
    $GLOBALS['css'] = 'main';
    $GLOBALS['shablon'] = 'main';
    $GLOBALS['title'] = 'Сортировка ' . $object['nameGen'];
    $GLOBALS['header'] = 'Сортировка ' . $object['nameGen'];
//--------------------------------------------------------------------------------
     if (isset($object['parent']) && isset($GLOBALS['parentElement'])) if (get($GLOBALS[$object['parent']]['mysqlTabeName'], 'ID', $GLOBALS['parentElement']) == null) exit("Не найден родительский объект"); else $parent = $GLOBALS['parentElement'];
//--------------------------------------------------------------------------------
?>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<div style="margin-bottom: 10px;" class='nav'>
     <?php /* <a href='/'>Главная</a> <?php if(isset($parent)) echo(" / <a href='".$GLOBALS[$object['parent']]['linkFolder']."'>".$GLOBALS[$object['parent']]['name'].'</a> / ' . "<a href='".str_replace('%id%', $parent, $GLOBALS[$object['parent']]['linkOne'])."'> ".(isset($GLOBALS[$object['parent']]['mainProper']) ? str_replace("%name%", get($GLOBALS[$object['parent']]['mysqlTabeName'], 'ID', $GLOBALS['parentElement'])[$GLOBALS[$object['parent']]['mainProper']], $GLOBALS[$object['parent']]['predName']) : 'Объект №'. $parent)."</a>"); ?> / <?php echo($object['name']); ?> */ ?>
    <a href='%base%'>Главная</a><?php echo(genNav($object, null, $parent??null,null)); ?>
</div>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<div class=" ">
    <div class="x_panel">
          <div class="x_title">
               <h2> <?php echo($object['name']); ?> <?php if(isset($parent)) echo("<small> ".(isset($GLOBALS[$object['parent']]['mainProper']) ? str_replace("%name%", get($GLOBALS[$object['parent']]['mysqlTabeName'], 'ID', $GLOBALS['parentElement'])[$GLOBALS[$object['parent']]['mainProper']], $GLOBALS[$object['parent']]['predName']) : 'Объект №'. $parent)." </small>"); ?> <small>Сортировка элементов</small></h2>
               <div class="clearfix"></div>
          </div>
          <div class="x_content">
               <table class="table table-hover">
                    <thead>
                         <tr>
                             <?php 
                               foreach ($object['items'] as $key => $value) if ($value['inList'] == 1) echo('<th style="cursor: pointer;" class=\'orderBy\' data-id="'.$key.'" >' . $value['Desc'] . ' <span class="o"></span></th>');
                             ?>
                         </tr>
                </thead><tbody class='p'>
                    
                    </tbody>
               </table>
               <ul class="pagination pages">
                    <li class="paginate_button active">
                         <a href="#">1</a>
                    </li>
                    <li class="paginate_button ">
                         <a href="#" aria-controls="datatable-checkbox" data-dt-idx="2" tabindex="0">2</a>
                    </li>
               </ul><br>
       
                    <?php 
                        if($object['isAddable']) { ?>
                            <a href='<?php echo(isset($parent)?$object['linkFolder'].'/'.$parent:$object['linkFolder']); ?>'><button type="submit" class="btn btn-default" style='margin-top: 0px;'>Назад</button></a> 
                    <?php  
                        }
                    ?>
              
        </div>
     </div>
</div>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<script>
(function(){
//--------------------------------------------------------------------------------------------
     var pages = $('.pages'); var page = 1; var atPage = 100000; var orderBy = 'ID'; var desc = 1;
     function upd(){
          //------------------------------------------------------
          // Собираем фильтры
          var args = {object: "<?php echo($keyObject); ?>"};
          $('.filter').each(function(index, el) { 
               var e = $(this);
               args[e.attr('data-row')] = e.val();
          }); args['page'] = page; args['atPage'] = atPage; args['order'] = orderBy; args['desc'] = desc;
          //------------------------------------------------------
          <?php if (isset($parent)){ ?>
               args['parent'] = <?php echo($parent); ?>;
          <?php } ?>
          //------------------------------------------------------
          $('.p').html("Идет загрузка");
          $('.pages').html("Идет загрузка");
          $('.numberMax').html('');
          //------------------------------------------------------
          // Отправляем запросик
          DoQuery('list', 'get', args, function(otv) {
               if (otv.html == null) {
                    alert('Ошибка сервера');
                    return;
               }
               $('.p').html(otv.html);
               $('.pages').html(otv.pages);
               $('.numberMax').text(""+otv.count+"");
               window.initSort();
               $(document).ready(onLoadObjects);
          });
          //------------------------------------------------------
     }
     //----------------------------------------------------------
     $(document).ready(function(){
          upd();
     });
     //----------------------------------------------------------
     window.goPage = function(p){
          page = p;
          upd();
     }
     //----------------------------------------------------------
     $(document).on('click', '.orderBy', function(){
          page = 1;
          $('.orderBy .o').html('');
          if (orderBy == $(this).attr('data-id')){
               if (desc == 1) desc = 0; else desc = 1;
          } else {
               orderBy = $(this).attr('data-id');;
               desc = 0;
          }
          //------------------------------------------------------
          var ht = '';
          if (desc == 1){
               ht = "<i class='fa fa-chevron-up'></i>";
          } else {
               ht = "<i class='fa fa-chevron-down'></i>";
          }
          $(this).find('.o').html(ht);
          //------------------------------------------------------
          upd();

          //------------------------------------------------------
     });
     //----------------------------------------------------------
     (function(){
        window.initSort = function(){
            
            $( ".p" ).sortable( { stop: update });
            $('.p').children().attr("onclick", "").click(function(event) {
                event.preventDefault();
            });

            function update(){
                var ids = [];
                $('.p').children().each(function(index, el) {
                    ids.push($(this).find('td').attr('data-id'));
                });
                $('.pages').html("Обновление в процессе");
                DoQuery("edit", "sort", {object: "<?php echo($keyObject); ?>", ids: JSON.stringify(ids)}, function(){
                    $('.pages').html("Обновление завершено");
                });
            }
        }

     })();
     //----------------------------------------------------------
})();
</script>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<?php 
 restoreDB();
 ?>
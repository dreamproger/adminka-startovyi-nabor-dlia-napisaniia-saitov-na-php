<?php 
	changeDB();
//--------------------------------------------------------------------------------
	$GLOBALS['css'] = 'main';
	$GLOBALS['shablon'] = 'main';
	$GLOBALS['title'] = 'Создание нового объекта ' . $object['name'];
	$GLOBALS['header'] = 'Создание нового объекта ' . $object['name'];
//--------------------------------------------------------------------------------
	$temp = time() * -1; 
//--------------------------------------------------------------------------------
	 if (isset($object['parent']) && isset($GLOBALS['parentElement'])) if (get($GLOBALS[$object['parent']]['mysqlTabeName'], 'ID', $GLOBALS['parentElement']) == null) exit("Не найден родительский объект"); else $parent = $GLOBALS['parentElement'];
//--------------------------------------------------------------------------------
?>
<div style="margin-bottom: 10px;" class='nav'>
	<a href='%base%'>Главная</a><?php echo(genNav($object, null, $parent??null,'Создать')); ?>
</div>
<div class="x_panel">
    <div class="x_title">
        <h2> <?php echo('Создание нового объекта <small>' . $object['name'] . '</small>'); ?> </h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <br>
        <form data-parsley-validate="" class="form-horizontal form-label-left" novalidate="">
        	<?php 
        		foreach ($object['items'] as $key => $value) if ($value['Type'] != 'Image') { 
        	?>
        	<div class="form-group">
            	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="f<?php echo($key); ?>"><?php echo($value['Desc']); ?></label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php echo(getEditValue($temp, $object, $key)); ?>
                    <span class='status'></span>
                </div>
            </div>

        	<?php 
        		}
        	?>
			
             
       
	        <div class="ln_solid"></div>
	      	<div class="form-group">
	        	<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	          		<button type="submit" class="btn btn-success">Сохранить</button>
	          		<br>
	          		<br>
	          		<span class='stat'>Картинки загружаются после создания объекта</span>
	        	</div>
	      	</div>
      	 </form>
    </div>
</div>
<script type="text/javascript" src='%base%vendors/ckeditor/ckeditor.js'></script>
<script>
$(document).ready(function() {
	moment.locale('ru');

	$('.date').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
    }, 
    function(start, end, label) {
        //var years = moment().diff(start, 'years');
        //alert("You are " + years + " years old.");
    });
});
var editors = {};
$(document).ready(function() {
	$('.htmlEditor').each(function(index, el) {
		
		editors[$(this).attr('data-row')] = CKEDITOR.replace(this);	
	});
});
//-------------------------------------------------------------------------------------------
$(document).ready(function() {
	$('input[type=file]').change(function(event) {
		var stat = $(this).parent().find('.status');
		var img = $(this).parent().find('img'); 
		var pa = $(this).parent();
		
		DoFileQuery('create', 'saveImage', {row: $(this).attr('data-row'), object: "<?php echo($keyObject); ?>", temp: <?php echo($temp); ?>}, function(proc){
			stat.text("Идет загрузка " + proc + '%');
		}, function(err){
			stat.text("Ошибка " + err);
		}, function(otv){
			stat.text("Загрузка файла успешно завершена");
			var url = img.attr('src').split('?')[0];

			img.attr('src', url+"?"+ Math.round(new Date().getTime()/1000));
		}, this);
	});
});
//-------------------------------------------------------------------------------------------
var tim = null;
$("form").submit(function(e) {
	clearTimeout(tim);
	var args = {object: "<?php echo($keyObject); ?>", temp: <?php echo($temp); ?>};
	$('.dataSave').each(function(index, el) {
		args[$(this).attr('data-row')] = $(this).val();
	});
	$('.dataSaveCheckBox').each(function(index, el) {
		args[$(this).attr('data-row')] = $(this).prop('checked') ? 1 : 0;
	});
	<?php if (isset($parent)){ ?>
	       args['parent'] = <?php echo($parent); ?>;
	  <?php } ?>
	$.each(editors, function(index, val) {
		args[index] = val.getData();
	});
	$('.geoMapEdit').each(function(index, el) {
		if (this.getCoordinates() == null) return;
		args[$(this).attr('data-row')] = this.getCoordinates();
	});
	DoQuery('create', 'create', args, function(otv){
		clearTimeout(tim);
		if (otv.response == 1){
			$('.stat').text("Сохранение завершено");
			tim = setTimeout(function(){ $('.stat').text(""); })
			window.location.href=otv.link;
		} else {
			$('.stat').text("Произошла ошибка, попробуйте позже");
		}
	});
    e.preventDefault();
});

$(document).ready(function(){onLoadObjects();});
</script>

<?php 
 restoreDB();
 ?>
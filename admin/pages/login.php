<?php 
//--------------------------------------------------------------------------------
	$GLOBALS['css'] = 'login';
	$GLOBALS['shablon'] = 'login';
	$GLOBALS['title'] = 'Вход';
	$GLOBALS['header'] = 'Вход';
//--------------------------------------------------------------------------------
?>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<div class="login_wrapper">
	<div class="animate form login_form">
  		<section class="login_content">
    		<form>
      			<h1>Авторизация</h1>
      			<div>
        			<input type="text" class="form-control mail" placeholder="Имя пользователя" required="" />
      			</div>
      			<div>
        			<input type="password" class="form-control pass" placeholder="Пароль" required="" />
      			</div>
      			<div>
              <div id="recaptcha" class="g-recaptcha" data-sitekey="<?php echo($GLOBALS['googleCapchaSiteKey']); ?>" data-callback="onSubmitReCaptcha" data-size="invisible"></div>
        			<a class="btn btn-default submit">Авторизация</a><br>
              <span class='state'></span>
      			</div>
      			<div class="clearfix"></div>
  				<div class="separator">
    				<div class="clearfix"></div>
   					<br />
   					<div>
     		 			<h1> <?php echo($GLOBALS['zag']); ?> </h1>
    				</div>
  				</div>
    		</form>
  		</section>
	</div>
</div>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
<script src='%base%libs/jquery.js'></script>
<script src='%base%libs/main.js'></script><script> var fold = "%base%"; </script>
<script src="https://www.google.com/recaptcha/api.js"></script>
<script>
  $('.submit').click(function(){ 
    grecaptcha.execute();
  });
  function onSubmitReCaptcha(token) {
     DoQuery('login', 'auth', {mail: $('.mail').val(), pass: $('.pass').val(), 'g-recaptcha-response': token }, function(data){
      grecaptcha.reset();
      if (data.response == '1'){ window.location.href = 'Главная'; } else if (data.response == '2') {
        $('.state').text('Неверные логин или пароль');
      } else if (data.response == '3'){
        $('.state').text('Слишком большое количество попыток, попробуйте через 5 минут');
      }
    });
  }
</script>
<!-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->
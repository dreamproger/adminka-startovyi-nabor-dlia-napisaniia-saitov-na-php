<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> %title% </title>

    <!-- Bootstrap -->
    <link href="%base%vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="%base%vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="%base%vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="%base%vendors/iCheck/skins/flat/green.css" rel="stylesheet">
	
    <!-- bootstrap-progressbar -->
    <link href="%base%vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="%base%vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="%base%vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <link href="%base%vendors/jquery.ui/jquery-ui.min.css" rel="stylesheet">
    <link href="%base%vendors/jquery.ui/jquery-ui.structure.min.css" rel="stylesheet">
    <link href="%base%vendors/jquery.ui/jquery-ui.theme.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="%base%css/custom.min.css" rel="stylesheet">
    <link href="%base%css/my.css" rel="stylesheet">
  </head>

  <body class="nav-md">
    <script src="%base%vendors/jquery/dist/jquery.min.js"></script>
    <script src="%base%vendors/jquery.ui/jquery-ui.min.js"></script>
    <script src="%base%libs/main.js"></script>
    <script> var fold = "%base%"; </script>
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="%base%" class="site_title"><span><?php echo($GLOBALS['zag']); ?></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="images/img.jpg" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Добро пожаловать,</span>
                <h2>%uName%</h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              %menu%
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="%base%выход">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

               
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          %content%
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            <?php echo($GLOBALS['zag']); ?>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <!-- Bootstrap -->
    <script src="%base%vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="%base%vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="%base%vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="%base%vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="%base%vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="%base%vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="%base%vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="%base%vendors/skycons/skycons.js"></script>
    <!-- Flot -->
    <script src="%base%vendors/Flot/jquery.flot.js"></script>
    <script src="%base%vendors/Flot/jquery.flot.pie.js"></script>
    <script src="%base%vendors/Flot/jquery.flot.time.js"></script>
    <script src="%base%vendors/Flot/jquery.flot.stack.js"></script>
    <script src="%base%vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="%base%vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="%base%vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="%base%vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="%base%vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="%base%vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="%base%vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="%base%vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="%base%vendors/moment/min/moment.min.js"></script>
    <script src="%base%vendors/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="%base%js/custom.min.js"></script>
    <script src="//api-maps.yandex.ru/2.1/?lang=ru_RU&load=package.full" type="text/javascript"></script>
    <script src="%base%js/my.js"></script>
  </body>
</html>

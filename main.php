<?php
	/* ---------------------------------------------------------------------------- */
		ini_set('display_errors',1);
		ini_set('display_startup_errors',1);
		error_reporting(-1);
	/* ---------------------------------------------------------------------------- */
		include(__DIR__.'/admin/settings.php');
		$GLOBALS['folder'] = __DIR__ . '/';
		$GLOBALS['url_full'] = $GLOBALS['constructHTTP'];
		$GLOBALS['url_short'] = '/';
		include(__DIR__.'/admin/datebase.php');
		include(__DIR__.'/admin/libs/functions.php');
	/* -------------------------------------------------------------------------- */
		$page = urldecode($_SERVER['REQUEST_URI'] );
		if ($GLOBALS['url_short'] == '/') $page = substr($page, 1); else $page = substr($page, strlen($GLOBALS['url_short']) );
		$pages = explode('/', $page);
		$page = $pages[0];
		$isZag = false;
	/* -------------------------------------------------------------------------- */
		switch (mb_strtolower($page)) {
			case 'проверка':	$page = page('test', $pages); 	break;
			case '': 			$page = page('index', $pages); 		break;
			default:			gopage('/'); 						break;
		}
	/* -------------------------------------------------------------------------- */
		$html = $page;
		$template = page('template', $pages);
	/* -------------------------------------------------------------------------- */
		$template = str_replace("%title%", $GLOBALS['title']??'', $template);
		$html = str_replace("%content%", $html, $template);
	/* -------------------------------------------------------------------------- */
		echo($html);
?>